import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserInvestmentDTO } from 'src/investment/dtos/create-user-investment.dto';
import { UserInvestmentDTO } from 'src/investment/dtos/user-investment.dto';
import { UserInvestment } from 'src/investment/entities/user-investment.entity';
import { InvestmentService } from 'src/investment/services/investment.service';
import { UserService } from 'src/user/user.service';
import { Repository } from 'typeorm';

@Injectable()
export class UserInvestmentService {
  constructor(
    @InjectRepository(UserInvestment)
    private readonly repository: Repository<UserInvestment>,
    private readonly investmentService: InvestmentService,
    private readonly userService: UserService,
  ) {}

  async findAllByUserId(userId: number): Promise<UserInvestment[]> {
    return this.repository.find({ user: { id: userId } });
  }

  async findUserInvestmentById(userId: number, investmentId: number): Promise<UserInvestment> {
    const [investment] = await this.repository.find({ id: investmentId, user: { id: userId } });
    return investment;
  }

  async create(userId: number, newInvestment: CreateUserInvestmentDTO): Promise<UserInvestment> {
    const user = await this.userService.findById(userId);
    const type = await this.investmentService.findTypeById(newInvestment.typeId);

    if (!type) {
      throw new Error('Type is invalid');
    }

    return this.repository.save({ ...newInvestment, user, type });
  }

  async update(
    userInvestment: UserInvestment,
    updatedInvesment: Partial<UserInvestmentDTO>,
  ): Promise<void> {
    await this.repository.update(userInvestment.id, { ...userInvestment, ...updatedInvesment });
  }

  async remove(userId: number, investmentId: number): Promise<void> {
    await this.repository.delete({ id: investmentId, user: { id: userId } });
  }
}
