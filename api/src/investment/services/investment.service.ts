import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { ExtractedInvestmentDTO } from 'src/investment/dtos/extracted-investment.dto';
import { InvestmentHistory } from 'src/investment/entities/investment-history.entity';
import { InvestmentType } from 'src/investment/entities/investment-type.entity';
import { InvestmentGroup } from 'src/investment/enums/investment-group.enum';
import { InvestmentHistoryRepository } from 'src/investment/repositories/investment-history.repository';
import { Repository } from 'typeorm';

@Injectable()
export class InvestmentService {
  constructor(
    @InjectRepository(InvestmentType)
    private readonly typesRepository: Repository<InvestmentType>,
    @InjectRepository(InvestmentHistoryRepository)
    private readonly historyRepository: InvestmentHistoryRepository,
  ) {}

  async findAllTypes(): Promise<InvestmentType[]> {
    return this.typesRepository.find();
  }

  async findTypeById(typeId: number): Promise<InvestmentType | undefined> {
    return this.typesRepository.findOne(typeId);
  }

  async getHistory(): Promise<InvestmentHistory[]> {
    return this.historyRepository.getHistory();
  }

  async findOrCreateType(investmentGroup: InvestmentGroup, name: string): Promise<InvestmentType> {
    const type = await this.typesRepository.findOne({ investmentGroup, name });

    if (type) {
      return type;
    }

    return this.typesRepository.save({
      investmentGroup,
      name,
    });
  }

  async saveExtractedInvestments(
    extractedAt: Date,
    extractedData: ExtractedInvestmentDTO[],
  ): Promise<void> {
    const investmentsToSave: InvestmentHistory[] = [];

    for (const investment of extractedData) {
      const type = await this.findOrCreateType(investment.group, investment.type);

      investmentsToSave.push(
        plainToClass(InvestmentHistory, {
          ...investment,
          type,
          fullName: `${investment.type} ${investment.year}`,
          extractedAt,
        }),
      );
    }

    await this.historyRepository.save(investmentsToSave);
  }
}
