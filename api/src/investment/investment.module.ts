import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InvestmentType } from 'src/investment/entities/investment-type.entity';
import { UserInvestment } from 'src/investment/entities/user-investment.entity';
import { UserInvestmentService } from 'src/investment/services/user-investment.service';
import { UserInvestmentController } from 'src/investment/controllers/user-investment.controller';
import { InvestmentController } from 'src/investment/controllers/investment.controller';
import { InvestmentService } from 'src/investment/services/investment.service';
import { UserModule } from 'src/user/user.module';
import { InvestmentHistoryRepository } from 'src/investment/repositories/investment-history.repository';

@Module({
  imports: [
    UserModule,
    TypeOrmModule.forFeature([InvestmentHistoryRepository, InvestmentType, UserInvestment]),
  ],
  controllers: [InvestmentController, UserInvestmentController],
  providers: [InvestmentService, UserInvestmentService],
  exports: [InvestmentService, UserInvestmentService],
})
export class InvestmentModule {}
