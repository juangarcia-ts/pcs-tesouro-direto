import { Body, Controller, Delete, Get, HttpStatus, NotFoundException, Param, Patch, Post } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { AuthUser } from 'src/auth/decorators/auth-user.decorator';
import { Authenticated } from 'src/auth/decorators/authenticated.decorator';
import { AuthUserDTO } from 'src/auth/dtos/auth-user.dto';
import { CreateUserInvestmentDTO } from 'src/investment/dtos/create-user-investment.dto';
import { UserInvestmentDTO } from 'src/investment/dtos/user-investment.dto';
import { UserInvestmentService } from 'src/investment/services/user-investment.service';

@Controller('/users/me/investments')
@ApiTags('User Investments')
export class UserInvestmentController {
  constructor(private readonly service: UserInvestmentService) {}

  @Get('/')
  @Authenticated()
  @ApiResponse({ status: HttpStatus.OK, description: 'Investments were successfully retrieved' })
  public async findAll(@AuthUser() user: AuthUserDTO): Promise<UserInvestmentDTO[]> {
    const investments = await this.service.findAllByUserId(user.sub);
    return plainToClass(UserInvestmentDTO, investments);
  }

  @Get('/:id')
  @Authenticated()
  @ApiResponse({ status: HttpStatus.OK, description: 'Investment was successfully retrieved' })
  public async findById(
    @AuthUser() user: AuthUserDTO,
    @Param('id') investmentId: number,
  ): Promise<UserInvestmentDTO | undefined> {
    const investment = await this.service.findUserInvestmentById(user.sub, investmentId);
    return plainToClass(UserInvestmentDTO, investment);
  }

  @Post()
  @Authenticated()
  @ApiResponse({ status: HttpStatus.OK, description: 'Investment was successfully updated' })
  public async create(
    @AuthUser() user: AuthUserDTO,
    @Body() newInvestment: CreateUserInvestmentDTO,
  ): Promise<UserInvestmentDTO> {
    const investment = await this.service.create(user.sub, newInvestment);
    return plainToClass(UserInvestmentDTO, investment);
  }

  @Patch('/:id')
  @Authenticated()
  @ApiResponse({ status: HttpStatus.OK, description: 'Investment was successfully updated' })
  @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Investment could not be found' })
  public async update(
    @AuthUser() user: AuthUserDTO,
    @Param('id') investmentId: number,
    @Body() updatedInvesment: Partial<UserInvestmentDTO>,
  ): Promise<void> {
    const existingInvestment = await this.service.findUserInvestmentById(user.sub, investmentId);

    if (!existingInvestment) {
      throw new NotFoundException();
    }

    await this.service.update(existingInvestment, updatedInvesment);
  }

  @Delete('/:id')
  @Authenticated()
  @ApiResponse({ status: HttpStatus.OK, description: 'Investment was successfully deleted' })
  public async remove(@AuthUser() user: AuthUserDTO, @Param('id') investmentId: number): Promise<void> {
    await this.service.remove(user.sub, investmentId);
  }
}
