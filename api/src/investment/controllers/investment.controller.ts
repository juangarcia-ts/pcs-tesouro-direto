import { Controller, Get, HttpStatus } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { InvestmentTypeDTO } from 'src/investment/dtos/investment-type.dto';
import { InvestmentHistoryDTO } from 'src/investment/dtos/investment-history.dto';
import { InvestmentService } from 'src/investment/services/investment.service';

@Controller('/investment')
@ApiTags('Investments')
export class InvestmentController {
  constructor(private readonly service: InvestmentService) {}

  @Get('/types')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Investments types were successfully retrieved',
  })
  async findAllTypes(): Promise<InvestmentTypeDTO[]> {
    const types = await this.service.findAllTypes();
    return plainToClass(InvestmentTypeDTO, types);
  }

  @Get('/history')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Investments history was successfully retrieved',
  })
  async getHistory(): Promise<InvestmentHistoryDTO[]> {
    const history = await this.service.getHistory();
    return plainToClass(InvestmentHistoryDTO, history);
  }
}
