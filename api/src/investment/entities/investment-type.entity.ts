import { InvestmentGroup } from 'src/investment/enums/investment-group.enum';
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity({ name: 'investment_type' })
export class InvestmentType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'text', name: 'investment_group' })
  investmentGroup: InvestmentGroup;

  @Column()
  name: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
