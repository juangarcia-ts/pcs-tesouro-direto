import { InvestmentType } from 'src/investment/entities/investment-type.entity';
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'investment_history' })
export class InvestmentHistory {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => InvestmentType, { eager: true })
  type: InvestmentType;

  @Column()
  year: number;

  @Column({ name: 'full_name' })
  fullName: string;

  @Column({ name: 'yield_rate' })
  yieldRate: string;

  @Column({ type: 'float', name: 'unit_price' })
  unitPrice: number;

  @Column({ type: 'float', name: 'min_value', nullable: true })
  minValue?: number;

  @Column({ name: 'expires_at' })
  expiresAt: Date;

  @CreateDateColumn({ name: 'extracted_at' })
  extractedAt: Date;
}
