import { InvestmentType } from 'src/investment/entities/investment-type.entity';
import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'user_investment' })
export class UserInvestment {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, { eager: true })
  user: User;

  @ManyToOne(() => InvestmentType, { eager: true })
  type: InvestmentType;

  @Column()
  name: string;

  @Column({ name: 'description', nullable: true })
  description?: string;

  @Column()
  amount: number;

  @Column({ type: 'float', name: 'paid_price' })
  paidPrice: number;

  @Column({ type: 'float', name: 'yield_rate' })
  yieldRate: number;

  @Column({ name: 'purchased_at' })
  purchasedAt: Date;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
