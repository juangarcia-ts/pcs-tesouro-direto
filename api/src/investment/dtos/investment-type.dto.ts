import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsEnum, IsNumber, IsString } from 'class-validator';
import { InvestmentGroup } from 'src/investment/enums/investment-group.enum';

@Exclude()
export class InvestmentTypeDTO {
  @ApiProperty()
  @IsNumber()
  @Expose()
  id: number;

  @ApiProperty()
  @IsEnum(InvestmentGroup)
  @Expose()
  investmentGroup: InvestmentGroup;

  @ApiProperty()
  @IsString()
  @Expose()
  name: string;
}
