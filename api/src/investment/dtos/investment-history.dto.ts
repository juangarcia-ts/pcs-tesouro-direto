import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsDate, IsNumber, IsOptional, IsString } from 'class-validator';
import { InvestmentTypeDTO } from 'src/investment/dtos/investment-type.dto';
import { InvestmentType } from 'src/investment/entities/investment-type.entity';

@Exclude()
export class InvestmentHistoryDTO {
  @ApiProperty()
  @IsNumber()
  @Expose()
  id: number;

  @ApiProperty()
  @Type(() => InvestmentTypeDTO)
  @Expose()
  type: InvestmentType;

  @ApiProperty()
  @IsNumber()
  @Expose()
  year: number;

  @ApiProperty()
  @IsString()
  @Expose()
  fullName: string;

  @ApiProperty()
  @IsString()
  @Expose()
  yieldRate: string;

  @ApiProperty()
  @IsNumber()
  @Expose()
  unitPrice: number;

  @ApiProperty()
  @IsNumber()
  @IsOptional()
  @Expose()
  minValue?: number;

  @ApiProperty()
  @IsDate()
  @Expose()
  expiresAt: Date;

  @ApiProperty()
  @IsDate()
  @Expose()
  extractedAt: Date;
}
