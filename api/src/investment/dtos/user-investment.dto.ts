import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsDate, IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';
import { InvestmentType } from 'src/investment/entities/investment-type.entity';
import { UserDTO } from 'src/user/dtos/user.dto';

@Exclude()
export class UserInvestmentDTO {
  @ApiProperty()
  @IsNumber()
  @Expose()
  id: number;

  @ApiProperty()
  @Type(() => UserDTO)
  @Expose()
  user: UserDTO;

  @ApiProperty()
  @IsEnum(InvestmentType)
  @Expose()
  type: InvestmentType;

  @ApiProperty()
  @IsString()
  @Expose()
  name: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  @Expose()
  description?: string;

  @ApiProperty()
  @IsNumber()
  @Expose()
  amount: number;

  @ApiProperty()
  @IsNumber()
  @Expose()
  paidPrice: number;

  @ApiProperty()
  @IsNumber()
  @Expose()
  yieldRate: number;

  @ApiProperty()
  @IsDate()
  @Expose()
  purchasedAt: Date;

  @ApiProperty()
  @IsDate()
  @Expose()
  createdAt: Date;

  @ApiProperty()
  @IsDate()
  @Expose()
  updatedAt: Date;
}
