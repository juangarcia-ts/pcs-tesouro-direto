import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Transform } from 'class-transformer';
import { IsDate, IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';

@Exclude()
export class CreateUserInvestmentDTO {
  @ApiProperty()
  @IsNumber()
  @Expose()
  typeId: number;

  @ApiProperty()
  @IsString()
  @Expose()
  name: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  @Expose()
  description?: string;

  @ApiProperty()
  @IsNumber()
  @Expose()
  amount: number;

  @ApiProperty()
  @IsNumber()
  @Expose()
  paidPrice: number;

  @ApiProperty()
  @IsNumber()
  @Expose()
  yieldRate: number;

  @ApiProperty()
  @IsDate()
  @Transform(({ value }) => new Date(value), { toClassOnly: true })
  @Expose()
  purchasedAt: Date;
}
