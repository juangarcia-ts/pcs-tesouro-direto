import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsDate, IsDecimal, IsEnum, IsNumber, IsString, ValidateIf } from 'class-validator';
import { InvestmentGroup } from 'src/investment/enums/investment-group.enum';

@Exclude()
export class ExtractedInvestmentDTO {
  @ApiProperty()
  @IsEnum(InvestmentGroup)
  @Expose()
  group: InvestmentGroup;

  @ApiProperty()
  @IsString()
  @Expose()
  type: string;

  @ApiProperty()
  @IsNumber()
  @Expose()
  year: number;

  @ApiProperty()
  @IsDecimal()
  @Expose()
  yieldRate: string;

  @ApiProperty()
  @IsDecimal()
  @Expose()
  unitPrice: number;

  @ApiProperty()
  @IsDecimal()
  @Expose()
  @ValidateIf(dto => dto.group === InvestmentGroup.DEPOSIT)
  minValue?: number;

  @ApiProperty()
  @IsDate()
  @Expose()
  expiresAt: Date;
}
