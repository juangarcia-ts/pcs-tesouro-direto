import { InvestmentHistory } from 'src/investment/entities/investment-history.entity';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(InvestmentHistory)
export class InvestmentHistoryRepository extends Repository<InvestmentHistory> {
  async getHistory(): Promise<InvestmentHistory[]> {
    const [result] = await this.query(
      'SELECT extracted_at FROM investment_history ORDER BY extracted_at DESC LIMIT 1',
    );

    if (!result?.extracted_at) {
      return [];
    }

    return this.find({ extractedAt: result.extracted_at });
  }
}
