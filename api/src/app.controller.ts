import { Controller, Get, HttpStatus, Query } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { NotificationType } from 'src/alert/enums/notification-type.enum';
import { InvestmentAlertService } from 'src/alert/investment-alert.service';
import { AuthUser } from 'src/auth/decorators/auth-user.decorator';
import { Authenticated } from 'src/auth/decorators/authenticated.decorator';
import { AuthUserDTO } from 'src/auth/dtos/auth-user.dto';
import { UserService } from 'src/user/user.service';

@Controller()
@ApiTags('Default')
export class AppController {
  constructor(
    private readonly alertService: InvestmentAlertService,
    private readonly userService: UserService,
  ) {}

  @Get()
  @ApiResponse({ status: HttpStatus.OK, description: 'App is healthy and running' })
  healthCheck(): Date {
    return new Date();
  }

  @Get('/alerts/test')
  @Authenticated()
  @ApiResponse({ status: HttpStatus.OK, description: 'Test alert was successfully sent' })
  public async testReceivement(
    @AuthUser() { sub: userId }: AuthUserDTO,
    @Query('type') type: NotificationType,
  ): Promise<void> {
    const user = await this.userService.findById(userId);
    await this.alertService.testReceivement(type, user);
  }
}
