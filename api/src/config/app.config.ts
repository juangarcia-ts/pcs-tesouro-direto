import { AppConfig } from 'src/config/config.interface';

export const appConfig = (): AppConfig => ({
  env: process.env.ENVIRONMENT,
  mongo: {
    uri: process.env.MONGO_CONNECTION_STRING,
  },
  redis: {
    host: process.env.REDIS_HOST,
    port: +process.env.REDIS_PORT,
  },
  vonage: {
    apiKey: process.env.VONAGE_API_KEY,
    apiSecret: process.env.VONAGE_API_SECRET,
  },
  sendGrid: {
    apiKey: process.env.SENDGRID_API_KEY,
  },
});
