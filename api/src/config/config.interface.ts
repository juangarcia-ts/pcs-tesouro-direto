export interface AppConfig {
  env: string;
  mongo: MongoConfig;
  redis: RedisConfig;
  vonage: VonageConfig;
  sendGrid: SendGridConfig;
}

export interface MongoConfig {
  uri: string;
}

export interface RedisConfig {
  host: string;
  port: number;
}

export interface VonageConfig {
  apiKey: string;
  apiSecret: string;
}

export interface SendGridConfig {
  apiKey: string;
}
