import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'blog_post' })
export class BlogPost {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  abstract: string;

  @Column()
  content: string;

  @Column({ name: 'cover_image_url' })
  coverImageUrl: string;

  @Column({ name: 'is_featured', default: false })
  isFeatured: boolean;

  @ManyToOne(() => User, { eager: true })
  createdBy: User;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
