import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class PostComment {
  @Prop()
  postId: number;

  @Prop()
  text: string;

  @Prop()
  replies: PostComment[];

  @Prop()
  createdAt: Date;

  @Prop(raw({ name: { type: String }, email: { type: String } }))
  createdBy: Record<string, string>;
}

export type PostCommentDocument = PostComment & Document;
export const PostCommentSchema = SchemaFactory.createForClass(PostComment);
