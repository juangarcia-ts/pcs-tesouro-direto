import { Body, Controller, Delete, Get, HttpStatus, NotFoundException, Param, Patch, Post } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { BlogPostService } from 'src/blog/services/blog-post.service';
import { BlogPostDTO } from 'src/blog/dtos/blog-post.dto';
import { plainToClass } from 'class-transformer';
import { CreateBlogPostDTO } from 'src/blog/dtos/create-blog-post.dto';
import { Authenticated } from 'src/auth/decorators/authenticated.decorator';
import { Role } from 'src/user/enums/role';
import { AuthUser } from 'src/auth/decorators/auth-user.decorator';
import { AuthUserDTO } from 'src/auth/dtos/auth-user.dto';

@Controller('/posts')
@ApiTags('Blog Posts')
export class BlogPostController {
  constructor(private readonly service: BlogPostService) {}

  @Get('/')
  @ApiResponse({ status: HttpStatus.OK, description: 'Posts were successfully retrieved' })
  public async findAll(): Promise<BlogPostDTO[]> {
    const posts = await this.service.findAll();
    return plainToClass(BlogPostDTO, posts);
  }

  @Get('/:id')
  @ApiResponse({ status: HttpStatus.OK, description: 'Post was successfully retrieved' })
  @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Post could not be found' })
  public async findById(@Param('id') id: number): Promise<BlogPostDTO | undefined> {
    const post = await this.service.findById(id);
    return plainToClass(BlogPostDTO, post);
  }

  @Post('/')
  @Authenticated([Role.ADMIN, Role.REDATOR])
  @ApiResponse({ status: HttpStatus.CREATED, description: 'Post was successfully created' })
  public async create(@AuthUser() user: AuthUserDTO, @Body() newPost: CreateBlogPostDTO): Promise<BlogPostDTO> {
    const post = await this.service.create(user.sub, newPost);
    return plainToClass(BlogPostDTO, post);
  }

  @Patch('/:id')
  @Authenticated([Role.REDATOR, Role.ADMIN])
  @ApiResponse({ status: HttpStatus.OK, description: 'Post was successfully updated' })
  @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Post could not be found' })
  public async update(@Param('id') id: number, @Body() updatedPost: Partial<BlogPostDTO>): Promise<void> {
    const existingPost = await this.service.findById(id);

    if (!existingPost) {
      throw new NotFoundException();
    }

    await this.service.update({ ...existingPost, ...updatedPost });
  }

  @Delete('/:id')
  @Authenticated([Role.REDATOR, Role.ADMIN])
  @ApiResponse({ status: HttpStatus.OK, description: 'Post was successfully deleted' })
  public async remove(@Param('id') id: number): Promise<void> {
    await this.service.remove(id);
  }
}
