import { BadRequestException, Body, Controller, Get, HttpStatus, Param, Patch, Post } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { Authenticated } from 'src/auth/decorators/authenticated.decorator';
import { AuthUser } from 'src/auth/decorators/auth-user.decorator';
import { PostCommentService } from 'src/blog/services/post-comment.service';
import { BlogPostService } from 'src/blog/services/blog-post.service';
import { PostCommentDTO } from 'src/blog/dtos/post-comment.dto';
import { AuthUserDTO } from 'src/auth/dtos/auth-user.dto';

@Controller('/posts/:postId/comments')
@ApiTags('Posts Comments')
export class PostCommentController {
  constructor(private readonly commentService: PostCommentService, private readonly postService: BlogPostService) {}

  @Get('/')
  @ApiResponse({ status: HttpStatus.OK, description: 'Comments were successfully retrieved' })
  public async findAll(@Param('postId') postId: number): Promise<PostCommentDTO[]> {
    const comments = await this.commentService.findAllByPost(postId);
    return plainToClass(PostCommentDTO, comments);
  }

  @Post('/')
  @Authenticated()
  @ApiResponse({ status: HttpStatus.CREATED, description: 'Comment was successfully created' })
  @ApiResponse({ status: HttpStatus.BAD_REQUEST, description: 'Post could not be found' })
  public async create(
    @AuthUser() user: AuthUserDTO,
    @Param('postId') postId: number,
    @Body('text') text: string,
  ): Promise<PostCommentDTO> {
    const post = await this.postService.findById(postId);

    if (!post) {
      throw new BadRequestException();
    }

    const comment = await this.commentService.create(user.sub, { postId, text });
    return plainToClass(PostCommentDTO, comment);
  }

  @Patch('/:id')
  @Authenticated()
  @ApiResponse({ status: HttpStatus.OK, description: 'Comment was successfully replied' })
  public async reply(
    @AuthUser() user: AuthUserDTO,
    @Param('id') id: string,
    @Param('postId') postId: number,
    @Body('text') text: string,
  ): Promise<void> {
    await this.commentService.reply(id, user.sub, { postId, text });
  }
}
