import { ApiProperty } from '@nestjs/swagger';
import { Expose, Exclude, Type } from 'class-transformer';
import { IsBoolean, IsDate, IsNumber, IsString } from 'class-validator';
import { UserDTO } from 'src/user/dtos/user.dto';

@Exclude()
export class BlogPostDTO {
  @ApiProperty()
  @IsNumber()
  @Expose()
  id: number;

  @ApiProperty()
  @IsString()
  @Expose()
  title: string;

  @ApiProperty()
  @IsString()
  @Expose()
  abstract: string;

  @ApiProperty()
  @IsString()
  @Expose()
  content: string;

  @ApiProperty()
  @IsString()
  @Expose()
  coverImageUrl: string;

  @ApiProperty()
  @IsBoolean()
  @Expose()
  isFeatured: boolean;

  @ApiProperty()
  @Type(() => UserDTO)
  @Expose()
  createdBy: UserDTO;

  @ApiProperty()
  @IsDate()
  @Expose()
  createdAt: Date;

  @ApiProperty()
  @IsDate()
  @Expose()
  updatedAt: Date;
}
