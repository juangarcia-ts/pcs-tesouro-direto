import { ApiProperty } from '@nestjs/swagger';
import { Expose, Exclude } from 'class-transformer';
import { IsNumber, IsString } from 'class-validator';

@Exclude()
export class CreatePostCommentDTO {
  @ApiProperty()
  @IsNumber()
  @Expose()
  postId: number;

  @ApiProperty()
  @IsString()
  @Expose()
  text: string;
}
