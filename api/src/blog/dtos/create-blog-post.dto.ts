import { ApiProperty } from '@nestjs/swagger';
import { Expose, Exclude } from 'class-transformer';
import { IsBoolean, IsString } from 'class-validator';

@Exclude()
export class CreateBlogPostDTO {
  @ApiProperty()
  @IsString()
  @Expose()
  title: string;

  @ApiProperty()
  @IsString()
  @Expose()
  abstract: string;

  @ApiProperty()
  @IsString()
  @Expose()
  content: string;

  @ApiProperty()
  @IsString()
  @Expose()
  coverImageUrl: string;

  @ApiProperty()
  @IsBoolean()
  @Expose()
  isFeatured: boolean;
}
