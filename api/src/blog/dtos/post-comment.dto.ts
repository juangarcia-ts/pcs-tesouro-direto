import { ApiProperty } from '@nestjs/swagger';
import { Expose, Exclude, Type } from 'class-transformer';
import { IsDate, IsNumber, IsString, IsUUID } from 'class-validator';

@Exclude()
export class PostCommentAuthorDTO {
  @ApiProperty()
  @IsString()
  @Expose()
  name: string;

  @ApiProperty()
  @IsString()
  @Expose()
  email: string;
}

@Exclude()
export class PostCommentDTO {
  @ApiProperty()
  @IsUUID()
  @Expose({ name: '_id' })
  id: string;

  @ApiProperty()
  @IsNumber()
  @Expose()
  postId: number;

  @ApiProperty()
  @IsString()
  @Expose()
  text: string;

  @ApiProperty()
  @Type(() => PostCommentDTO)
  @Expose()
  replies: PostCommentDTO[];

  @ApiProperty()
  @IsDate()
  @Expose()
  createdAt: Date;

  @ApiProperty()
  @Type(() => PostCommentAuthorDTO)
  @Expose()
  createdBy: PostCommentAuthorDTO;
}
