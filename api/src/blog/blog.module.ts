import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlogPostController } from 'src/blog/controllers/blog-post.controller';
import { PostCommentController } from 'src/blog/controllers/post-comment.controller';
import { BlogPost } from 'src/blog/entities/blog-post.entity';
import { PostComment, PostCommentSchema } from 'src/blog/schemas/post-comment.schema';
import { BlogPostService } from 'src/blog/services/blog-post.service';
import { PostCommentService } from 'src/blog/services/post-comment.service';
import { UserModule } from 'src/user/user.module';

@Module({
  imports: [
    UserModule,
    TypeOrmModule.forFeature([BlogPost]),
    MongooseModule.forFeature([{ name: PostComment.name, schema: PostCommentSchema }]),
  ],
  controllers: [BlogPostController, PostCommentController],
  providers: [BlogPostService, PostCommentService],
})
export class BlogModule {}
