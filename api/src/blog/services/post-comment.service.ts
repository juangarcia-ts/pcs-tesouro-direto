import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreatePostCommentDTO } from 'src/blog/dtos/create-post-comment.dto';
import { PostComment, PostCommentDocument } from 'src/blog/schemas/post-comment.schema';
import { UserService } from 'src/user/user.service';

@Injectable()
export class PostCommentService {
  constructor(
    @InjectModel(PostComment.name)
    private readonly model: Model<PostCommentDocument>,
    private readonly userService: UserService,
  ) {}

  public async findAllByPost(postId: number): Promise<PostComment[]> {
    const documents = await this.model.find({ postId });
    return documents.map(doc => doc.toObject());
  }

  public async create(userId: number, comment: CreatePostCommentDTO): Promise<PostComment> {
    const newComment = await this.buildComment(comment, userId);
    const [document] = await this.model.insertMany([newComment]);
    return document.toObject();
  }

  public async reply(id: string, userId: number, comment: CreatePostCommentDTO): Promise<void> {
    const reply = await this.buildComment(comment, userId);
    await this.model.updateOne({ _id: id }, { $push: { replies: reply } });
  }

  private async buildComment(comment: CreatePostCommentDTO, userId: number) {
    const user = await this.userService.findById(userId);

    return {
      ...comment,
      createdAt: new Date(),
      createdBy: {
        name: user.name,
        email: user.email,
      },
    };
  }
}
