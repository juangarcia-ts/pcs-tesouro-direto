import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BlogPostDTO } from 'src/blog/dtos/blog-post.dto';
import { CreateBlogPostDTO } from 'src/blog/dtos/create-blog-post.dto';
import { BlogPost } from 'src/blog/entities/blog-post.entity';
import { UserService } from 'src/user/user.service';
import { Repository } from 'typeorm';

@Injectable()
export class BlogPostService {
  constructor(
    @InjectRepository(BlogPost)
    private readonly repository: Repository<BlogPost>,
    private readonly userService: UserService,
  ) {}

  public async findAll(): Promise<BlogPost[]> {
    return this.repository.find();
  }

  public async findById(id: number): Promise<BlogPost | undefined> {
    return this.repository.findOne(id);
  }

  public async create(userId: number, post: CreateBlogPostDTO): Promise<BlogPost> {
    const user = await this.userService.findById(userId);
    return this.repository.save({ ...post, createdBy: user });
  }

  public async update(updatedPost: Partial<BlogPostDTO>): Promise<void> {
    await this.repository.update(updatedPost.id, updatedPost);
  }

  public async remove(id: number): Promise<void> {
    await this.repository.delete(id);
  }
}
