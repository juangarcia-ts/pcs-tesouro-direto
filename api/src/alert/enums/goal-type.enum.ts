export enum GoalType {
  HIGHER = 'Higher',
  EQUAL = 'Equal',
  LOWER = 'Lower',
}
