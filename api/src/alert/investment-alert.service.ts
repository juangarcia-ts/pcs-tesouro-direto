import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateInvestmentAlertDTO } from 'src/alert/dtos/create-investment-alert.dto';
import { InvestmentAlertDTO } from 'src/alert/dtos/investment-alert.dto';
import { InvestmentAlert } from 'src/alert/entities/investment-alert.entity';
import { NotificationType } from 'src/alert/enums/notification-type.enum';
import { AlertStrategy } from 'src/alert/strategies/alert.strategy';
import { EmailStrategy } from 'src/alert/strategies/email.strategy';
import { SmsStrategy } from 'src/alert/strategies/sms.strategy';
import { UserInvestmentDTO } from 'src/investment/dtos/user-investment.dto';
import { UserDTO } from 'src/user/dtos/user.dto';
import { Repository } from 'typeorm';

@Injectable()
export class InvestmentAlertService {
  constructor(
    @InjectRepository(InvestmentAlert)
    private readonly repository: Repository<InvestmentAlert>,
    private readonly emailStrategy: EmailStrategy,
    private readonly smsStrategy: SmsStrategy,
  ) {}

  getAlertStrategy(type: NotificationType): AlertStrategy {
    const strategies: Record<NotificationType, AlertStrategy> = {
      [NotificationType.EMAIL]: this.emailStrategy,
      [NotificationType.SMS]: this.smsStrategy,
    };

    return strategies[type];
  }

  async findAllByInvestmentId(userId: number, investmentId: number): Promise<InvestmentAlert[]> {
    return this.repository.find({ investment: { id: investmentId, user: { id: userId } } });
  }

  async findInvestmentAlertById(
    userId: number,
    investmentId: number,
    alertId: number,
  ): Promise<InvestmentAlert> {
    const [alert] = await this.repository.find({
      id: alertId,
      investment: { id: investmentId, user: { id: userId } },
    });
    return alert;
  }

  async create(
    investment: UserInvestmentDTO,
    newAlert: CreateInvestmentAlertDTO,
  ): Promise<InvestmentAlert> {
    return this.repository.save({ ...newAlert, investment });
  }

  async update(alert: InvestmentAlert, updatedAlert: Partial<InvestmentAlertDTO>): Promise<void> {
    await this.repository.update(alert.id, { ...alert, ...updatedAlert });
  }

  async remove(userId: number, investmentId: number, alertId: number): Promise<void> {
    await this.repository.delete({
      id: alertId,
      investment: { id: investmentId, user: { id: userId } },
    });
  }

  async testReceivement(type: NotificationType, user: UserDTO): Promise<void> {
    const strategy = this.getAlertStrategy(type);
    await strategy.testReceivement(user);
  }
}
