import { GoalType } from 'src/alert/enums/goal-type.enum';
import { NotificationType } from 'src/alert/enums/notification-type.enum';
import { UserInvestment } from 'src/investment/entities/user-investment.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'investment_alert' })
export class InvestmentAlert {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => UserInvestment, { eager: true })
  investment: UserInvestment;

  @Column({ type: 'text', name: 'goal_type' })
  goalType: GoalType;

  @Column({ name: 'goal_type' })
  priceGoal: number;

  @Column({ type: 'text', name: 'notification_type' })
  notificationType: NotificationType;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
