import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsDate, IsEnum, IsNumber } from 'class-validator';
import { GoalType } from 'src/alert/enums/goal-type.enum';
import { NotificationType } from 'src/alert/enums/notification-type.enum';
import { UserInvestmentDTO } from 'src/investment/dtos/user-investment.dto';

@Exclude()
export class InvestmentAlertDTO {
  @ApiProperty()
  @IsNumber()
  @Expose()
  id: number;

  @ApiProperty()
  @Type(() => UserInvestmentDTO)
  @Expose()
  investment: UserInvestmentDTO;

  @ApiProperty()
  @IsEnum(GoalType)
  @Expose()
  goalType: GoalType;

  @ApiProperty()
  @IsNumber()
  @Expose()
  priceGoal: number;

  @ApiProperty()
  @IsEnum(NotificationType)
  @Expose()
  notificationType: NotificationType;

  @ApiProperty()
  @IsDate()
  @Expose()
  createdAt: Date;

  @ApiProperty()
  @IsDate()
  @Expose()
  updatedAt: Date;
}
