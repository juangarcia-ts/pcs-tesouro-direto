import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsEnum, IsNumber } from 'class-validator';
import { GoalType } from 'src/alert/enums/goal-type.enum';
import { NotificationType } from 'src/alert/enums/notification-type.enum';

@Exclude()
export class CreateInvestmentAlertDTO {
  @ApiProperty()
  @IsEnum(GoalType)
  @Expose()
  goalType: GoalType;

  @ApiProperty()
  @IsNumber()
  @Expose()
  priceGoal: number;

  @ApiProperty()
  @IsEnum(NotificationType)
  @Expose()
  notificationType: NotificationType;
}
