import { UserDTO } from 'src/user/dtos/user.dto';

export abstract class AlertStrategy {
  abstract sendAlert(receiver: UserDTO, message: string): Promise<void>;

  abstract testReceivement(receiver: UserDTO): Promise<void>;
}
