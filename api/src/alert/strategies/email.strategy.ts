import { Injectable, Logger } from '@nestjs/common';
import { AlertStrategy } from 'src/alert/strategies/alert.strategy';
import { UserDTO } from 'src/user/dtos/user.dto';
import { MailService } from '@sendgrid/mail';

@Injectable()
export class EmailStrategy extends AlertStrategy {
  constructor(private readonly emailClient: MailService) {
    super();
  }

  async sendAlert(receiver: UserDTO, message: string): Promise<void> {
    await this.emailClient.send({
      from: '"MeuTesouro" <juan.silva@uniriotec.br>', // sender address
      to: receiver.email,
      subject: 'Hello ✔', // Subject line
      text: message,
    });

    await this.sendEmail(receiver.email, 'Alerta de preço!', message);
  }

  async testReceivement(receiver: UserDTO): Promise<void> {
    await this.sendEmail(
      receiver.email,
      'Confirmação de recebimento',
      'Se você recebeu esse e-mail, você está apto para receber alertas',
    );
  }

  private async sendEmail(to: string, subject: string, text: string): Promise<void> {
    const email = {
      from: '"MeuTesouro" <juan.silva@uniriotec.br>',
      to,
      subject,
      text,
    };

    return new Promise((resolve, reject) => {
      this.emailClient.send(email, false, (err, result) => {
        if (err) {
          reject(err);
          return;
        }

        Logger.log(result);
        resolve();
      });
    });
  }
}
