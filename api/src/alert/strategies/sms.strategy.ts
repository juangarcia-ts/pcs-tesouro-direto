import { Inject, Injectable, Logger } from '@nestjs/common';
import Vonage from '@vonage/server-sdk';
import { SMS_PROVIDER } from 'src/alert/investment-alert.constants';
import { AlertStrategy } from 'src/alert/strategies/alert.strategy';
import { UserDTO } from 'src/user/dtos/user.dto';

@Injectable()
export class SmsStrategy extends AlertStrategy {
  constructor(@Inject(SMS_PROVIDER) private readonly smsClient: Vonage) {
    super();
  }

  async sendAlert(receiver: UserDTO, message: string): Promise<void> {
    await this.sendSms(receiver.phone, message);
  }

  async testReceivement(receiver: UserDTO): Promise<void> {
    await this.sendSms(
      receiver.phone,
      'Confirmação de recebimento. Se você recebeu esse SMS, você está apto a receber mensagens',
    );
  }

  async sendSms(phone: string, message: string): Promise<void> {
    return new Promise((resolve, reject) => {
      this.smsClient.message.sendSms('Vonage APIs', phone, message, {}, (err, result) => {
        if (err) {
          reject(err);
          return;
        }

        Logger.log(result);
        resolve();
      });
    });
  }
}
