import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InvestmentAlert } from 'src/alert/entities/investment-alert.entity';
import { InvestmentAlertController } from 'src/alert/investment-alert.controller';
import { InvestmentAlertService } from 'src/alert/investment-alert.service';
import { EmailStrategy } from 'src/alert/strategies/email.strategy';
import { SmsStrategy } from 'src/alert/strategies/sms.strategy';
import { InvestmentModule } from 'src/investment/investment.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { SMS_PROVIDER } from 'src/alert/investment-alert.constants';
import { MailService } from '@sendgrid/mail';
import * as sendGrid from '@sendgrid/mail';
import * as Vonage from '@vonage/server-sdk';

const EmailClient = {
  provide: MailService,
  inject: [ConfigService],
  useFactory: (config: ConfigService) => {
    sendGrid.setApiKey(config.get('sendGrid.apiKey'));
    return sendGrid;
  },
};

const SmsClient = {
  provide: SMS_PROVIDER,
  inject: [ConfigService],
  useFactory: (config: ConfigService) => {
    const { apiKey, apiSecret } = config.get('vonage');

    return new (Vonage as any)({
      apiKey,
      apiSecret,
    });
  },
};

@Module({
  imports: [ConfigModule, InvestmentModule, TypeOrmModule.forFeature([InvestmentAlert])],
  controllers: [InvestmentAlertController],
  providers: [InvestmentAlertService, SmsStrategy, EmailStrategy, SmsClient, EmailClient],
  exports: [InvestmentAlertService],
})
export class AlertModule {}
