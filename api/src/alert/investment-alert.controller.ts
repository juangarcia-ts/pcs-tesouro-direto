import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  NotFoundException,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { CreateInvestmentAlertDTO } from 'src/alert/dtos/create-investment-alert.dto';
import { InvestmentAlertDTO } from 'src/alert/dtos/investment-alert.dto';
import { InvestmentAlertService } from 'src/alert/investment-alert.service';
import { AuthUser } from 'src/auth/decorators/auth-user.decorator';
import { Authenticated } from 'src/auth/decorators/authenticated.decorator';
import { AuthUserDTO } from 'src/auth/dtos/auth-user.dto';
import { UserInvestmentService } from 'src/investment/services/user-investment.service';

@Controller('/users/me/investments/:investmentId/alerts')
@ApiTags('Investment Alerts')
export class InvestmentAlertController {
  constructor(
    private readonly alertService: InvestmentAlertService,
    private readonly userInvestmentService: UserInvestmentService,
  ) {}

  @Get('/')
  @Authenticated()
  @ApiResponse({ status: HttpStatus.OK, description: 'Alerts were successfully retrieved' })
  public async findAll(
    @AuthUser() user: AuthUserDTO,
    @Param('investmentId') investmentId: number,
  ): Promise<InvestmentAlertDTO[]> {
    const alerts = await this.alertService.findAllByInvestmentId(user.sub, investmentId);
    return plainToClass(InvestmentAlertDTO, alerts);
  }

  @Get('/:id')
  @Authenticated()
  @ApiResponse({ status: HttpStatus.OK, description: 'Alert was successfully retrieved' })
  public async findById(
    @AuthUser() user: AuthUserDTO,
    @Param('investmentId') investmentId: number,
    @Param('id') alertId: number,
  ): Promise<InvestmentAlertDTO | undefined> {
    const alert = await this.alertService.findInvestmentAlertById(user.sub, investmentId, alertId);
    return plainToClass(InvestmentAlertDTO, alert);
  }

  @Post()
  @Authenticated()
  @ApiResponse({ status: HttpStatus.OK, description: 'Alert was successfully updated' })
  @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Investment could not be found' })
  public async create(
    @AuthUser() user: AuthUserDTO,
    @Param('investmentId') investmentId: number,
    @Body() newAlert: CreateInvestmentAlertDTO,
  ): Promise<InvestmentAlertDTO> {
    const investment = await this.userInvestmentService.findUserInvestmentById(
      user.sub,
      investmentId,
    );

    if (!investment) {
      throw new NotFoundException();
    }

    const alert = await this.alertService.create(investment, newAlert);
    return plainToClass(InvestmentAlertDTO, alert);
  }

  @Patch('/:id')
  @Authenticated()
  @ApiResponse({ status: HttpStatus.OK, description: 'Alert was successfully updated' })
  @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Alert could not be found' })
  public async update(
    @AuthUser() user: AuthUserDTO,
    @Param('investmentId') investmentId: number,
    @Param('id') alertId: number,
    @Body() updatedAlert: Partial<InvestmentAlertDTO>,
  ): Promise<void> {
    const existingAlert = await this.alertService.findInvestmentAlertById(
      user.sub,
      investmentId,
      alertId,
    );

    if (!existingAlert) {
      throw new NotFoundException();
    }

    await this.alertService.update(existingAlert, updatedAlert);
  }

  @Delete('/:id')
  @Authenticated()
  @ApiResponse({ status: HttpStatus.OK, description: 'Alert was successfully deleted' })
  public async remove(
    @AuthUser() user: AuthUserDTO,
    @Param('investmentId') investmentId: number,
    @Param('id') alertId: number,
  ): Promise<void> {
    await this.alertService.remove(user.sub, investmentId, alertId);
  }
}
