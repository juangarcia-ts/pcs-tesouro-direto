import { Module, Provider } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import * as Redis from 'ioredis';
import { CACHE_PROVIDER } from 'src/cache/cache.constants';

const redisClient: Provider = {
  provide: CACHE_PROVIDER,
  inject: [ConfigService],
  useFactory: (config: ConfigService) => {
    const { host, port } = config.get('redis');
    return new Redis(port, host);
  },
};

@Module({
  imports: [ConfigModule],
  providers: [redisClient],
  exports: [redisClient],
})
export class CacheModule {}
