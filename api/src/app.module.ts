import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AlertModule } from 'src/alert/investment-alert.module';
import { AuthModule } from 'src/auth/auth.module';
import { BlogModule } from 'src/blog/blog.module';
import { appConfig } from 'src/config/app.config';
import { CrawlerModule } from 'src/crawler/crawler.module';
import { InvestmentModule } from 'src/investment/investment.module';
import { UserModule } from 'src/user/user.module';
import { AppController } from './app.controller';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    AlertModule,
    AuthModule,
    BlogModule,
    CrawlerModule,
    InvestmentModule,
    UserModule,
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({ load: [appConfig] }),
    TypeOrmModule.forRoot(),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        return { uri: config.get('mongo.uri') };
      },
    }),
  ],
  controllers: [AppController],
})
export class AppModule {}
