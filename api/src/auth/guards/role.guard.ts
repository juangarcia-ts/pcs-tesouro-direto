import { CanActivate, ExecutionContext, ForbiddenException, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Role } from 'src/user/enums/role';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const req = context.switchToHttp().getRequest();
    const roles = this.reflector.get<Role[]>('roles', context.getHandler());

    if (!roles || roles.includes(req.user?.role)) {
      return true;
    }

    throw new ForbiddenException();
  }
}
