import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class CryptoService {
  hashPassword(plainPassword: string): string {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(plainPassword, salt);
  }

  comparePasswords(passwordHash: string, plainPassword: string): boolean {
    return bcrypt.compareSync(plainPassword, passwordHash);
  }
}
