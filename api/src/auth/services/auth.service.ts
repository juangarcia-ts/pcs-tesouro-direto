import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { plainToClass } from 'class-transformer';
import { AccessTokenDTO, TokenPayloadDTO } from 'src/auth/dtos/access-token.dto';
import { AuthUserDTO } from 'src/auth/dtos/auth-user.dto';
import { SignUpDTO } from 'src/auth/dtos/sign-up.dto';
import { CryptoService } from 'src/auth/services/crypto.service';
import { CreateUserDTO } from 'src/user/dtos/create-user.dto';
import { UserDTO } from 'src/user/dtos/user.dto';
import { User } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/user.service';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private cryptoService: CryptoService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<User | null> {
    const user = await this.userService.findByEmail(email);

    if (!user) {
      return null;
    }

    const isValid = this.cryptoService.comparePasswords(user.passwordHash, password);

    if (!isValid) {
      return null;
    }

    return user;
  }

  async login(user: UserDTO): Promise<AccessTokenDTO> {
    const payload: TokenPayloadDTO = {
      sub: user.id,
      email: user.email,
      role: user.role,
    };

    return {
      accessToken: this.jwtService.sign(payload),
    };
  }

  async signUp(newAccount: SignUpDTO): Promise<UserDTO> {
    const user = plainToClass(CreateUserDTO, {
      ...newAccount,
      passwordHash: this.cryptoService.hashPassword(newAccount.password),
    });

    return this.userService.create(user);
  }
}
