import { Body, Controller, HttpStatus, Post, Req, UseGuards } from '@nestjs/common';
import { ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { AuthService } from 'src/auth/services/auth.service';
import { AccessTokenDTO } from 'src/auth/dtos/access-token.dto';
import { LocalAuthGuard } from 'src/auth/guards/local.guard';
import { UserDTO } from 'src/user/dtos/user.dto';
import { SignUpDTO } from 'src/auth/dtos/sign-up.dto';
import { LoginDTO } from 'src/auth/dtos/login.dto';

@Controller('/auth')
@ApiTags('Authentication')
export class AuthController {
  constructor(private readonly service: AuthService) {}

  @Post('/login')
  @UseGuards(LocalAuthGuard)
  @ApiBody({ type: LoginDTO })
  @ApiResponse({ status: HttpStatus.OK, description: 'Login was Successful' })
  @ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: 'Credentials not valid' })
  public async login(@Req() req: { user: UserDTO }): Promise<AccessTokenDTO> {
    const token = this.service.login(req.user);
    return plainToClass(AccessTokenDTO, token);
  }

  @Post('/signUp')
  @ApiResponse({ status: HttpStatus.CREATED, description: 'Account was successfully created' })
  @ApiResponse({ status: HttpStatus.CONFLICT, description: 'E-mail is already taken' })
  public async signUp(@Body() newAccount: SignUpDTO): Promise<UserDTO> {
    const user = await this.service.signUp(newAccount);
    return plainToClass(UserDTO, user);
  }
}
