import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { AuthUserDTO } from 'src/auth/dtos/auth-user.dto';

export const AuthUser = createParamDecorator((_: unknown, ctx: ExecutionContext) => {
  const req = ctx.switchToHttp().getRequest();
  return req.user as AuthUserDTO;
});
