import { applyDecorators, HttpStatus, SetMetadata, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiResponse } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
import { RoleGuard } from 'src/auth/guards/role.guard';
import { Role } from 'src/user/enums/role';

export const Authenticated = (allowedRoles?: Role[]): any => {
  return applyDecorators(
    ApiBearerAuth(),
    ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: 'Access token is missing or is not valid' }),
    ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'User has not enough permission to perform this action' }),
    SetMetadata('roles', allowedRoles),
    UseGuards(JwtAuthGuard, RoleGuard),
  );
};
