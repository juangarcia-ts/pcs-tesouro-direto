import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsNumber, IsString } from 'class-validator';
import { LoginDTO } from 'src/auth/dtos/login.dto';

@Exclude()
export class SignUpDTO extends LoginDTO {
  @ApiProperty()
  @IsString()
  @Expose()
  name: string;

  @ApiProperty()
  @IsString()
  @Expose()
  phone: string;
}
