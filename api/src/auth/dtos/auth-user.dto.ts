import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsEmail, IsEnum, IsNumber } from 'class-validator';
import { Role } from 'src/user/enums/role';

@Exclude()
export class AuthUserDTO {
  @ApiProperty()
  @IsNumber()
  @Expose()
  sub: number;

  @ApiProperty()
  @IsEmail()
  @Expose()
  email: string;

  @ApiProperty()
  @IsEnum(Role)
  @Expose()
  role: Role;

  @ApiProperty()
  @IsNumber()
  @Expose()
  iat: number;

  @ApiProperty()
  @IsNumber()
  @Expose()
  exp: number;
}
