import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsEmail, IsEnum, IsNumber, IsString } from 'class-validator';
import { Role } from 'src/user/enums/role';

@Exclude()
export class TokenPayloadDTO {
  @ApiProperty()
  @IsNumber()
  @Expose()
  sub: number;

  @ApiProperty()
  @IsEmail()
  @Expose()
  email: string;

  @ApiProperty()
  @IsEnum(Role)
  @Expose()
  role: Role;
}

@Exclude()
export class AccessTokenDTO {
  @ApiProperty()
  @IsString()
  @Expose()
  accessToken: string;
}
