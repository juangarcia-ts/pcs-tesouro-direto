import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateInvestTypeTable1618702906599 implements MigrationInterface {
  name = 'CreateInvestTypeTable1618702906599';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "investment_type" ("id" SERIAL NOT NULL, "investment_group" text NOT NULL, "name" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_5509533eeb8175ec89b8ca478b6" PRIMARY KEY ("id"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "investment_type"`);
  }
}
