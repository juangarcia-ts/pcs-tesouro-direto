import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateUserInvestmentTable1618702999268 implements MigrationInterface {
  name = 'CreateUserInvestmentTable1618702999268';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "user_investment" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "description" character varying, "amount" integer NOT NULL, "paid_price" double precision NOT NULL, "yield_rate" double precision NOT NULL, "purchased_at" TIMESTAMP NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "userId" integer, "typeId" integer, CONSTRAINT "PK_4c58401c44d177d4e1181768c98" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_investment" ADD CONSTRAINT "FK_06d84cd79b94e4b78727ecd35cf" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_investment" ADD CONSTRAINT "FK_8d4bd8f442d1a4bce76b4948327" FOREIGN KEY ("typeId") REFERENCES "investment_type"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "user_investment" DROP CONSTRAINT "FK_8d4bd8f442d1a4bce76b4948327"`);
    await queryRunner.query(`ALTER TABLE "user_investment" DROP CONSTRAINT "FK_06d84cd79b94e4b78727ecd35cf"`);
    await queryRunner.query(`DROP TABLE "user_investment"`);
  }
}
