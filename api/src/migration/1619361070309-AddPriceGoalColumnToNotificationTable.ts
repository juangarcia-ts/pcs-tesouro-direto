import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddPriceGoalColumnToNotificationTable1619361070309 implements MigrationInterface {
  name = 'AddPriceGoalColumnToNotificationTable1619361070309';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "notification" ADD "price_goal" double precision NOT NULL`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "notification" DROP COLUMN "price_goal"`);
  }
}
