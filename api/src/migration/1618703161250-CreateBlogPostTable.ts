import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateBlogPostTable1618703161250 implements MigrationInterface {
  name = 'CreateBlogPostTable1618703161250';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "blog_post" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "abstract" character varying NOT NULL, "content" character varying NOT NULL, "cover_image_url" character varying NOT NULL, "is_featured" boolean NOT NULL DEFAULT false, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "createdById" integer, CONSTRAINT "PK_694e842ad1c2b33f5939de6fede" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "blog_post" ADD CONSTRAINT "FK_9cd75ef9a7cab7d9fcfdf9077cd" FOREIGN KEY ("createdById") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "blog_post" DROP CONSTRAINT "FK_9cd75ef9a7cab7d9fcfdf9077cd"`,
    );
    await queryRunner.query(`DROP TABLE "blog_post"`);
  }
}
