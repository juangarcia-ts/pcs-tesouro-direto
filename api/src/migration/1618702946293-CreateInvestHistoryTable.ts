import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateInvestHistoryTable1618702946293 implements MigrationInterface {
  name = 'CreateInvestHistoryTable1618702946293';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "investment_history" ("id" SERIAL NOT NULL, "year" integer NOT NULL, "full_name" character varying NOT NULL, "yield_rate" character varying NOT NULL, "unit_price" double precision NOT NULL, "min_value" double precision, "expires_at" TIMESTAMP NOT NULL, "extracted_at" TIMESTAMP NOT NULL DEFAULT now(), "typeId" integer, CONSTRAINT "PK_556a7b99998b6587454dada0d45" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "investment_history" ADD CONSTRAINT "FK_339e7882cafc32f0f799a80aaa1" FOREIGN KEY ("typeId") REFERENCES "investment_type"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "investment_history" DROP CONSTRAINT "FK_339e7882cafc32f0f799a80aaa1"`,
    );
    await queryRunner.query(`DROP TABLE "investment_history"`);
  }
}
