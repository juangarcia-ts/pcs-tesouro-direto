import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsBoolean, IsEnum, IsNumber, IsString } from 'class-validator';
import { Role } from 'src/user/enums/role';

@Exclude()
export class UserDTO {
  @ApiProperty()
  @IsNumber()
  @Expose()
  id: number;

  @ApiProperty()
  @IsEnum(Role)
  @Expose()
  role: Role;

  @ApiProperty()
  @IsString()
  @Expose()
  name: string;

  @ApiProperty()
  @IsString()
  @Expose()
  email: string;

  @ApiProperty()
  @IsString()
  @Expose()
  phone: string;

  @ApiProperty()
  @IsBoolean()
  @Expose()
  isVerified: boolean;
}
