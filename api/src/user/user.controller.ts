import { Body, Controller, Delete, HttpStatus, Patch } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthUser } from 'src/auth/decorators/auth-user.decorator';
import { Authenticated } from 'src/auth/decorators/authenticated.decorator';
import { AuthUserDTO } from 'src/auth/dtos/auth-user.dto';
import { UserDTO } from 'src/user/dtos/user.dto';
import { UserService } from 'src/user/user.service';

@Controller('/users')
@ApiTags('Users')
export class UserController {
  constructor(private readonly service: UserService) {}

  @Patch('/me')
  @Authenticated()
  @ApiResponse({ status: HttpStatus.OK, description: 'User was successfully updated' })
  public async update(@AuthUser() user: AuthUserDTO, @Body() updatedUser: Partial<UserDTO>): Promise<void> {
    await this.service.update(user.sub, updatedUser);
  }

  @Delete('/me')
  @Authenticated()
  @ApiResponse({ status: HttpStatus.OK, description: 'User was successfully deleted' })
  public async remove(@AuthUser() user: AuthUserDTO): Promise<void> {
    await this.service.remove(user.sub);
  }
}
