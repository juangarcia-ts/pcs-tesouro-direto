import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDTO } from 'src/user/dtos/create-user.dto';
import { UserDTO } from 'src/user/dtos/user.dto';
import { User } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly repository: Repository<User>,
  ) {}

  public async findById(id: number): Promise<User | undefined> {
    return this.repository.findOne(id);
  }

  public async findByEmail(email: string): Promise<User | undefined> {
    return this.repository.findOne({ email });
  }

  public async create(user: CreateUserDTO): Promise<User> {
    return this.repository.save(user);
  }

  public async update(userId: number, updatedUser: Partial<UserDTO>): Promise<void> {
    const user = await this.findById(userId);
    await this.repository.update(userId, { ...user, ...updatedUser });
  }

  public async remove(id: number): Promise<void> {
    await this.repository.delete(id);
  }
}
