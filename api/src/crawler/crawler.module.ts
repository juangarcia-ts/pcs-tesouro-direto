import { Module } from '@nestjs/common';
import { CacheModule } from 'src/cache/cache.module';
import { TesouroDiretoJob } from 'src/crawler/tesouro-direto.job';
import { InvestmentModule } from 'src/investment/investment.module';

@Module({
  imports: [InvestmentModule, CacheModule],
  providers: [TesouroDiretoJob],
})
export class CrawlerModule {}
