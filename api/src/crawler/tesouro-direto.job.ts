import { Inject, Injectable, Logger } from '@nestjs/common';
import { InvestmentService } from 'src/investment/services/investment.service';
import { Cron, CronExpression } from '@nestjs/schedule';
import * as puppeteer from 'puppeteer';
import { ExtractedInvestmentDTO } from 'src/investment/dtos/extracted-investment.dto';
import { convertCurrencyToNumber, generateExecutionId, parseDate } from 'src/crawler/crawler.utils';
import { InvestmentGroup } from 'src/investment/enums/investment-group.enum';
import { Redis } from 'ioredis';
import { CACHE_PROVIDER } from 'src/cache/cache.constants';

@Injectable()
export class TesouroDiretoJob {
  private readonly url = 'https://www.tesourodireto.com.br/titulos/precos-e-taxas.htm';

  constructor(
    @Inject(CACHE_PROVIDER)
    private readonly cache: Redis,
    private readonly investmentService: InvestmentService,
  ) {}

  @Cron(CronExpression.EVERY_30_SECONDS)
  public async setup(): Promise<void> {
    await this.crawl();
  }

  async crawl(): Promise<void> {
    const executionId = generateExecutionId();
    const hasAlreadyCrawled = await this.cache.get(executionId);

    if (hasAlreadyCrawled) {
      return;
    }

    const browser = await puppeteer.launch({ headless: true, defaultViewport: null });
    const page = await browser.newPage();

    await page.exposeFunction('currency', convertCurrencyToNumber);
    await page.exposeFunction('date', parseDate);

    await page.goto(this.url, { waitUntil: 'load', timeout: 0 });

    try {
      const investments = await this.extract(page);
      const extractedAt = new Date();

      for (const group in investments) {
        await this.investmentService.saveExtractedInvestments(extractedAt, investments[group]);
      }

      await this.cache.set(executionId, JSON.stringify({ extractedAt }));
    } catch (err) {
      Logger.error(err);
    } finally {
      await page.close();
      await browser.close();
    }
  }

  private async extract(
    page: puppeteer.Page,
  ): Promise<Record<InvestmentGroup, ExtractedInvestmentDTO[]>> {
    await page.waitForSelector('.td-lds-spinner', { hidden: true });
    await page.click('.td-switcher__button--table');

    return {
      [InvestmentGroup.DEPOSIT]: await this.extractDepositGroup(page),
      [InvestmentGroup.WITHDRAW]: await this.extractWithdrawGroup(page),
    };
  }

  private extractDepositGroup = (page: puppeteer.Page): Promise<ExtractedInvestmentDTO[]> => {
    return page.evaluate(async (investmentGroup: InvestmentGroup) => {
      const { currency, date } = window as any;

      const results: ExtractedInvestmentDTO[] = [];
      const cards = Array.from(
        document.querySelectorAll('#td-precos_taxas-tab_1 .td-invest-table__card'),
      );

      for (const card of cards) {
        const [investmentType] = card
          .querySelector('.td-invest-table__name__text')
          .textContent.split('\t');

        const investmentYear = card.querySelector('.td-invest-table__name__year').textContent;

        const columns = Array.from(card.querySelectorAll('.td-invest-table__col__text'));
        const [yieldRate, minValue, unitPrice, expiresAt] = columns.map(col => col.textContent);

        results.push({
          group: investmentGroup,
          yieldRate,
          type: investmentType.trim(),
          year: +investmentYear,
          minValue: await currency(minValue),
          unitPrice: await currency(unitPrice),
          expiresAt: await date(expiresAt),
        });
      }

      return results;
    }, InvestmentGroup.DEPOSIT);
  };

  private extractWithdrawGroup = (page: puppeteer.Page): Promise<ExtractedInvestmentDTO[]> => {
    return page.evaluate(async (investmentGroup: InvestmentGroup) => {
      const { currency, date } = window as any;

      const results: ExtractedInvestmentDTO[] = [];
      const cards = Array.from(
        document.querySelectorAll('#td-precos_taxas-tab_2 .td-invest-table__card'),
      );

      for (const card of cards) {
        const [investmentType] = card
          .querySelector('.td-invest-table__name__text')
          .textContent.split('\t');

        const investmentYear = card.querySelector('.td-invest-table__name__year').textContent;

        const columns = Array.from(card.querySelectorAll('.td-invest-table__col__text'));
        const [yieldRate, unitPrice, expiresAt] = columns.map(col => col.textContent);

        results.push({
          group: investmentGroup,
          yieldRate,
          type: investmentType.trim(),
          year: +investmentYear,
          unitPrice: await currency(unitPrice),
          expiresAt: await date(expiresAt),
        });
      }

      return results;
    }, InvestmentGroup.WITHDRAW);
  };
}
