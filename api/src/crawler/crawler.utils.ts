import { parse, format } from 'date-fns';

export const generateExecutionId = (): string => {
  return format(new Date(), 'dd/MM/yyyy');
};

export const convertCurrencyToNumber = (currency: string): number => {
  const cleanText = currency
    .trim()
    .replace('.', '')
    .replace(',', '.')
    .replace('R$', '');

  return parseFloat(cleanText);
};

export const parseDate = (date: string, pattern = 'dd/MM/yyyy'): Date => {
  return parse(date, pattern, new Date());
};
