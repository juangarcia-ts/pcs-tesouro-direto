import { createMuiTheme } from "@material-ui/core";

export const customTheme = createMuiTheme({
  palette: {
    common: {
      black: "#191F23",
    },
    error: {
      main: "#CE4242",
    },
    primary: {
      main: "#237E91",
      dark: "#044F63",
    },
    text: {
      primary: "#191f23",
      secondary: "#536470",
      hint: "#CCC",
    },
  },
  typography: {
    fontFamily: "Alata",
    htmlFontSize: 16,
  },
});
