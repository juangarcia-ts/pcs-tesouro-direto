export interface PostCommentAuthorDTO {
  name: string;
  email: string;
}

export interface PostCommentDTO {
  id: string;
  postId: number;
  text: string;
  replies: PostCommentDTO[];
  createdAt: Date;
  createdBy: PostCommentAuthorDTO;
}
