import { Role } from "./authDto";

export interface UserDTO {
  id: number;
  role: Role;
  name: string;
  email: string;
  phone: string;
  isVerified: boolean;
}
