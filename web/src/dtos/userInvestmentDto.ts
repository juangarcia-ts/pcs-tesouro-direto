import { InvestmentTypeDTO } from "./investmentTypeDto";

export interface UserInvestmentDTO {
  id: number;
  type: InvestmentTypeDTO;
  name: string;
  description?: string;
  amount: number;
  paidPrice: number;
  yieldRate: number;
  purchasedAt: Date;
  createdAt: Date;
  updatedAt: Date;
}

export interface CreateUserInvestmentDTO {
  type: InvestmentTypeDTO;
  description?: string;
  amount: number;
  paidPrice: number;
  yieldRate: number;
  purchasedAt: Date;
}
