import { InvestmentGroup } from "../enums/investmentGroupEnum";
import { InvestmentTypeDTO } from "./investmentTypeDto";

export interface InvestmentHistoryDTO {
  [InvestmentGroup.WITHDRAW]: ExtractedInvestmentDTO[];
  [InvestmentGroup.DEPOSIT]: ExtractedInvestmentDTO[];
}

export interface ExtractedInvestmentDTO {
  id: number;
  type: InvestmentTypeDTO;
  year: number;
  fullName: string;
  yieldRate: string;
  unitPrice: number;
  minValue?: number;
  expiresAt: Date;
  extractedAt: Date;
}
