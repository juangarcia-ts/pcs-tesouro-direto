export interface PostDTO {
  id: number;
  title: string;
  abstract: string;
  content: string;
  coverImageUrl: string;
  isFeatured: boolean;
  createdBy: any; // TODO: Add UserDTO
  createdAt: Date;
  updatedAt: Date;
}
