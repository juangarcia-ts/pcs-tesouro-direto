import { GoalType } from "../enums/goalTypeEnum";
import { NotificationType } from "../enums/notificationTypeEnum";
import { UserInvestmentDTO } from "./userInvestmentDto";

export interface InvestmentAlertDTO {
  id: number;
  investment: UserInvestmentDTO;
  goalType: GoalType;
  priceGoal: number;
  notificationType: NotificationType;
  createdAt: Date;
  updatedAt: Date;
}

export interface CreateInvestmentAlertDTO {
  priceGoal: number;
  goalType: GoalType;
  notificationType: NotificationType;
}
