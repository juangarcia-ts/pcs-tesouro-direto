import { InvestmentGroup } from "../enums/investmentGroupEnum";

export class InvestmentTypeDTO {
  id: number;
  investmentGroup: InvestmentGroup;
  name: string;
}
