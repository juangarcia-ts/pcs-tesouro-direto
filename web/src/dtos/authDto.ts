export interface LoginDTO {
  email: string;
  password: string;
}

export interface SignUpDTO extends LoginDTO {
  name: string;
  phone: string;
}

export interface AuthUserDTO {
  email: string;
  role: Role;
}

export enum Role {
  CUSTOMER = "CUSTOMER",
  REDATOR = "REDATOR",
  ADMIN = "ADMIN",
}
