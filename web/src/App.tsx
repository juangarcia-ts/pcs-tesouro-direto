import "./assets/global.css";
import "typeface-alata";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import {
  Landing,
  BlogIndex,
  BlogPost,
  AdminPost,
  NotFound,
  Auth,
  Home,
  InvestorProfile,
  UserInvestments,
  Alerts,
  InvestmentSimulator,
  Settings,
} from "./pages";
import { Navbar } from "./components";
import { CssBaseline, ThemeProvider } from "@material-ui/core";
import { customTheme } from "./theme/customTheme";
import { useLoggedUser } from "./hooks/useLoggedUser";
import { AuthContext } from "./contexts/authContext";
import { useState } from "react";
import { getAccessToken } from "./utils/token";

const App = () => {
  const [token, setToken] = useState(getAccessToken());

  const renderRoutes = () => {
    return (
      <Switch>
        {/* Público */}
        <Route exact path="/" component={Landing} />
        <Route exact path="/blog" component={BlogIndex} />
        <Route exact path="/blog/:id" component={BlogPost} />
        <Route exact path="/entrar" component={Auth} />

        {/* Logado */}
        <PrivateRoute exact path="/pagina-inicial" component={Home} />
        <PrivateRoute exact path="/minha-conta" component={Settings} />
        <PrivateRoute exact path="/meus-alertas" component={Alerts} />
        <PrivateRoute exact path="/meus-titulos" component={UserInvestments} />
        <PrivateRoute
          exact
          path="/simular-rendimentos"
          component={InvestmentSimulator}
        />
        <PrivateRoute
          exact
          path="/simular-perfil"
          component={InvestorProfile}
        />

        {/* Administrador */}
        <PrivateRoute exact path="/admin/blog" component={AdminPost} />
        <Route component={NotFound} />
      </Switch>
    );
  };

  return (
    <AuthContext.Provider value={{ token, setToken }}>
      <ThemeProvider theme={customTheme}>
        <CssBaseline />
        <Router>
          <Navbar />
          {renderRoutes()}
        </Router>
      </ThemeProvider>
    </AuthContext.Provider>
  );
};

const PrivateRoute = ({ component: Comp, ...rest }) => {
  const loggedUser = useLoggedUser();

  return (
    <Route
      {...rest}
      render={(props) =>
        loggedUser ? (
          <Comp {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/entrar",
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};

export default App;
