import { Button, Grid, Typography } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { Layout } from "../../components";
import useStyles from "./NotFound.styles";

export const NotFound = () => {
  const classes = useStyles();
  const router = useHistory();

  const redirectToHomePage = () => {
    router.push("/");
  };

  return (
    <Layout>
      <Grid
        container
        direction="column"
        alignItems="center"
        justify="center"
        spacing={2}
        className={classes.container}
      >
        <Grid item>
          <Typography variant="h3">404 - Página Não Encontrada</Typography>
        </Grid>

        <Grid item>
          <Button
            color="primary"
            variant="contained"
            onClick={redirectToHomePage}
          >
            Voltar para a página inicial
          </Button>
        </Grid>
      </Grid>
    </Layout>
  );
};
