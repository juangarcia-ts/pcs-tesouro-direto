import { makeStyles } from "@material-ui/core";

export default makeStyles(() => ({
  container: {
    height: "calc(100vh - 100px)",
  },
}));
