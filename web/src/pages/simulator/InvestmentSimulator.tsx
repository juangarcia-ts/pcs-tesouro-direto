import { Layout, Simulator } from "../../components";

export const InvestmentSimulator = () => {
  return (
    <Layout>
      <Simulator />
    </Layout>
  );
};
