import { HomeMenu, Layout } from "../../components";

export const Home = () => {
  return (
    <Layout>
      <HomeMenu />
    </Layout>
  );
};
