import landingBackground from "../../assets/images/landing-bg.png";
import { makeStyles, Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
  hero: {
    backgroundImage: `url(${landingBackground})`,
    backgroundSize: "cover",
    width: "100vw",
    height: "40vh",
  },

  title: {
    color: theme.palette.common.white,
    padding: 32,
  },
}));
