import { useEffect, useState } from "react";
import { InvestmentService } from "../../services";
import { InvestmentHistory, Loading } from "../../components";
import useStyles from "./Landing.styles";
import { Container, Grid, Typography } from "@material-ui/core";
import {
  ExtractedInvestmentDTO,
  InvestmentHistoryDTO,
} from "../../dtos/investmentHistoryDto";
import { InvestmentGroup } from "../../enums/investmentGroupEnum";

export const Landing = () => {
  const classes = useStyles();

  const [history, setHistory] = useState<InvestmentHistoryDTO>(null);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    fetchHistory();
  }, []);

  const fetchHistory = async () => {
    const { data } = await InvestmentService.getHistory();
    const history = buildHistory(data);
    setHistory(history);
    setLoading(false);
  };

  const buildHistory = (
    data: ExtractedInvestmentDTO[]
  ): InvestmentHistoryDTO => {
    const history = {
      [InvestmentGroup.WITHDRAW]: [],
      [InvestmentGroup.DEPOSIT]: [],
    };

    for (const investment of data) {
      history[investment.type.investmentGroup].push(investment);
    }

    return history;
  };

  return (
    <main>
      <Grid
        container
        alignItems="center"
        justify="center"
        className={classes.hero}
      >
        <Grid item>
          <Typography variant="h3" className={classes.title}>
            Acompanhar seus investimentos nunca se tornou tão prático!
          </Typography>
        </Grid>
      </Grid>

      <Container>
        {isLoading ? <Loading /> : <InvestmentHistory history={history} />}
      </Container>
    </main>
  );
};
