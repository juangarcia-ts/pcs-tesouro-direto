import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { format } from "date-fns";
import { Layout, Loading } from "../../components";
import { BlogService } from "../../services";
import { PostDTO } from "../../dtos/postDto";
import { Grid, Typography, Link, TextField } from "@material-ui/core";
import * as css from "./Styled";
import { PostCommentDTO } from "../../dtos/postCommentDto";

export const BlogPost = () => {
  const urlParams: { id?: string } = useParams();
  const [post, setPost] = useState<PostDTO>(null);
  const [replyingPost, setReplyingPost] = useState(null);
  const [commentText, setCommentText] = useState("");
  const [comments, setComments] = useState<PostCommentDTO[]>([]);

  useEffect(() => {
    fetchPost();
    fetchComments();
  }, []);

  const fetchPost = async () => {
    const { data } = await BlogService.findPostById(+urlParams.id);
    setPost(data);
  };

  const fetchComments = async () => {
    const { data } = await BlogService.findAllComments(+urlParams.id);
    setComments(data);
  };

  const createComment = async () => {
    await BlogService.createComment(+urlParams.id, commentText);
    setCommentText("");
    fetchComments();
  };

  const replyPost = async () => {
    await BlogService.replyComment(+urlParams.id, replyingPost, commentText);
    setCommentText("");
    setReplyingPost(null);
    fetchComments();
  };

  const renderComments = () => {
    if (comments.length === 0) {
      return <Typography>Não há comentários</Typography>;
    }

    return comments.map((comment) => (
      <Grid key={comment.id} item xs={12}>
        {renderSingleComment(comment)}

        {comment.replies.map((reply) => renderSingleComment(reply, true))}
      </Grid>
    ));
  };

  const renderSingleComment = (comment: PostCommentDTO, isReply?: boolean) => {
    const { createdAt, createdBy, text } = comment;
    const date = format(new Date(createdAt), "dd/MM/yyyy");

    return (
      <Grid
        container
        justify="space-between"
        style={{ marginLeft: isReply ? 30 : 10 }}
      >
        <Grid item>
          <Typography variant="overline">{`[${date}] ${createdBy.name}: `}</Typography>
          <Typography variant="caption">{`${text} `}</Typography>
        </Grid>

        {!isReply && (
          <Grid item>
            <Link onClick={() => setReplyingPost(comment.id)}>
              <Typography variant="overline">Responder</Typography>
            </Link>
          </Grid>
        )}
      </Grid>
    );
  };

  if (!post) {
    return <Loading />;
  }

  return (
    <Layout>
      <Grid container direction="column" spacing={2}>
        <Grid item xs={12}>
          <Typography variant="h3" component="h1">
            {post.title}
          </Typography>
        </Grid>

        <Grid item xs={12}>
          <css.PostDate className="text-right">
            Por: Meu Tesouro | Publicado em:{" "}
            {format(new Date(post.createdAt), "dd/MM/yyyy")}
          </css.PostDate>
        </Grid>

        <Grid item xs={12}>
          <css.PostCover src={post.coverImageUrl} alt={post.title} />
        </Grid>

        <Grid item xs={12}>
          <div dangerouslySetInnerHTML={{ __html: post.content }} />
        </Grid>

        <Grid item xs={12}>
          <Typography variant="h6">Comentários</Typography>

          <Grid container>{renderComments()}</Grid>

          <TextField
            placeholder="Novo comentário"
            value={commentText}
            onChange={(e) => setCommentText(e.target.value)}
            onKeyPress={(e) => {
              if (e.key === "Enter" && commentText.length > 0) {
                if (replyingPost) {
                  replyPost();
                } else {
                  createComment();
                }
              }
            }}
          />
        </Grid>
      </Grid>
    </Layout>
  );
};
