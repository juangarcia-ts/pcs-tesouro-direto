import { useEffect, useState } from "react";
import { Grid, Link, Typography } from "@material-ui/core";
import { BlogService } from "../../services";
import { format } from "date-fns";
import { PostDTO } from "../../dtos/postDto";
import { Loading, Layout, Carrousel } from "../../components";
import { useHistory } from "react-router-dom";
import * as css from "./Styled";

export const BlogIndex = () => {
  const router = useHistory();
  const [isLoading, toggleLoading] = useState(true);
  const [posts, setPosts] = useState([] as PostDTO[]);

  useEffect(() => {
    fetchPosts();
  }, []);

  const fetchPosts = async () => {
    try {
      const { data } = await BlogService.findAllPosts();
      setPosts(data);
    } finally {
      toggleLoading(false);
    }
  };

  const openPost = (postId: number) => {
    router.push(`/blog/${postId}`);
  };

  const renderHighlights = () => {
    const highlights = posts.filter((post) => post.isFeatured);

    if (!highlights.length) {
      return null;
    }

    return (
      <Carrousel>
        {highlights.map((highlight, index) => (
          <css.HighlightWrapper key={index}>
            <css.HighlightImage
              src={highlight.coverImageUrl}
              alt={highlight.title}
            />
            <css.HighlightTitle onClick={() => openPost(highlight.id)}>
              {highlight.title}
            </css.HighlightTitle>
          </css.HighlightWrapper>
        ))}
      </Carrousel>
    );
  };

  const renderPosts = () => {
    return posts.map((post: PostDTO) => {
      return (
        <css.PostBox key={post.id}>
          <Link href={`/blog/${post.id}`}>
            <Typography variant="h4" component="h1">
              {post.title}
            </Typography>
          </Link>

          <css.PostDate>
            {`Por: Meu Tesouro | Publicado em: ${format(
              new Date(post.createdAt),
              "dd/MM/yyyy HH:mm"
            )}`}
          </css.PostDate>

          <Grid container spacing={4}>
            <Grid item xs={12} sm={12} md={5} lg={5}>
              <css.PostImage
                src={post.coverImageUrl}
                alt={post.title}
                onClick={() => openPost(post.id)}
              />
            </Grid>

            <Grid item xs={12} sm={12} md={7} lg={7}>
              <Typography variant="body1">{post.abstract}</Typography>
              <Link onClick={() => openPost(post.id)}>Leia mais...</Link>
            </Grid>
          </Grid>
        </css.PostBox>
      );
    });
  };

  if (isLoading) {
    return <Loading />;
  }

  return (
    <Layout>
      {posts.length ? (
        <>
          {renderHighlights()}
          {renderPosts()}
        </>
      ) : (
        <Typography>Não há nenhuma publicação cadastrada.</Typography>
      )}
    </Layout>
  );
};
