import { makeStyles } from "@material-ui/core";
import investorProfileBackground from "../../assets/images/investor-profile-cover.jpg";

export default makeStyles(() => ({
  coverImage: {
    backgroundImage: `url(${investorProfileBackground})`,
    backgroundSize: "cover",
    backgroundPosition: "50% 25%",
    height: 300,
    width: "100%",
    margin: "3% 0",
  },

  button: {
    marginTop: 24,
  },
}));
