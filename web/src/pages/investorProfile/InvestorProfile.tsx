import { Button, Grid, Typography } from "@material-ui/core";
import { useState } from "react";
import { Layout, Quiz } from "../../components";
import { quizQuestions } from "../../utils/data";
import useStyles from "./InvestorProfile.styles";

export const InvestorProfile = () => {
  const classes = useStyles();
  const [hasQuizStarted, setQuizAsStarted] = useState(false);

  const startQuiz = () => {
    setQuizAsStarted(true);
  };

  return (
    <Layout>
      {hasQuizStarted ? (
        <Quiz questions={quizQuestions} />
      ) : (
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography variant="h3">
              O que é o perfil do investidor?
            </Typography>
          </Grid>

          <Grid item xs={12}>
            <div className={classes.coverImage} />
          </Grid>

          <Grid item xs={12}>
            <Typography variant="body1" component="p">
              Cada investidor possui objetivos diferentes para o eu patrimônio
              e, além disso, tolera riscos diferentes. Dessa forma, a
              identificação do perfil tem por finalidade encontrar uma
              distribuição de investimentos que seja compatível com esse perfil,
              sem gerar ansiedade, mal-estar ou qualquer desconforto ao
              investidor.
            </Typography>
          </Grid>

          <Grid item xs={12}>
            <Typography variant="body1" component="p">
              Existem 3 possíveis perfis que você conhecerá ao final do quiz:
              <b> Conservador, Moderado ou Agressivo</b>
            </Typography>
          </Grid>

          <Grid item xs={12}>
            <Button
              color="primary"
              variant="contained"
              className={classes.button}
              onClick={startQuiz}
            >
              Inicie agora!
            </Button>
          </Grid>
        </Grid>
      )}
    </Layout>
  );
};
