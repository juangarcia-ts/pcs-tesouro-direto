import { makeStyles } from "@material-ui/core";
import authBackground from "../../assets/images/auth.jpeg";

export default makeStyles(() => ({
  container: {
    marginTop: 80,
  },

  column: {
    position: "absolute",
    width: "50vw",
    minHeight: "calc(100vh - 80px)",
  },

  left: {
    left: 0,
    backgroundPosition: "center center",
    backgroundSize: "cover",
    backgroundImage: `url(${authBackground})`,
  },

  right: {
    right: 0,
  },

  form: {
    padding: 64,
  },
}));
