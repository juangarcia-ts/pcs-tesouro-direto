import { useContext, useState } from "react";
import { Loading } from "../../components";
import { AuthService } from "../../services";
import { useForm } from "react-hook-form";
import { Button, Grid, Typography, TextField, Link } from "@material-ui/core";
import { LoginDTO, Role, SignUpDTO } from "../../dtos/authDto";
import { Redirect } from "react-router-dom";
import clsx from "clsx";
import useStyles from "./Auth.styles";
import { setAccessToken } from "../../utils/token";
import { useLoggedUser } from "../../hooks/useLoggedUser";
import { AuthContext } from "../../contexts/authContext";

export enum Form {
  LOGIN,
  SIGN_UP,
}

const formText = {
  [Form.LOGIN]: {
    title: "Bem-vindo de volta",
    toggleText: "Ainda não é um usuário",
    toggleLink: "Cadastre-se",
    button: "Entrar",
  },
  [Form.SIGN_UP]: {
    title: "Crie sua conta",
    toggleText: "Já é cadastrado?",
    toggleLink: "Faça login",
    button: "Confirmar",
  },
};

export const Auth = () => {
  const classes = useStyles();

  const loggedUser = useLoggedUser();
  const { setToken } = useContext(AuthContext);

  const [isLoading, toggleLoading] = useState(false);
  const [currentForm, setForm] = useState(Form.LOGIN);

  const { register, handleSubmit: submitWrapper, formState } = useForm();

  const toggleForm = () => {
    setForm(currentForm === Form.SIGN_UP ? Form.LOGIN : Form.SIGN_UP);
  };

  const handleSubmit = async (data) => {
    toggleLoading(true);

    if (currentForm === Form.SIGN_UP) {
      await AuthService.signUp(data as SignUpDTO);
    }

    await login(data as LoginDTO);
    toggleLoading(false);
  };

  const login = async (data: LoginDTO) => {
    const {
      data: { accessToken },
    } = await AuthService.login(data);

    setAccessToken(accessToken);
    setToken(accessToken);
  };

  if (loggedUser) {
    return (
      <Redirect
        to={loggedUser.role === Role.ADMIN ? "/admin/blog" : "/pagina-inicial"}
      />
    );
  }

  return (
    <div className={classes.container}>
      {isLoading && <Loading />}

      <div className={clsx(classes.column, classes.left)} />

      <div className={clsx(classes.column, classes.right)}>
        <form onSubmit={submitWrapper(handleSubmit)} className={classes.form}>
          <Grid container direction="column" justify="center" spacing={4}>
            <Grid item xs={12}>
              <Typography variant="h4">
                {formText[currentForm].title}
              </Typography>
            </Grid>

            <Grid item xs={12}>
              <Typography variant="overline">E-mail</Typography>

              <TextField
                fullWidth
                type="email"
                {...register("email", { required: true })}
              />
            </Grid>

            {currentForm === Form.SIGN_UP && (
              <>
                <Grid item xs={12}>
                  <Typography variant="overline">Nome</Typography>

                  <TextField
                    fullWidth
                    {...register("name", { required: true })}
                  />
                </Grid>

                <Grid item xs={12}>
                  <Typography variant="overline">Telefone</Typography>

                  <TextField
                    fullWidth
                    type="tel"
                    {...register("phone", { required: true })}
                  />
                </Grid>
              </>
            )}

            <Grid item xs={12}>
              <Typography variant="overline">Senha</Typography>

              <TextField
                fullWidth
                type="password"
                {...register("password", { required: true })}
              />
            </Grid>

            {currentForm === Form.SIGN_UP && (
              <Grid item xs={12}>
                <Typography variant="overline">Confirmação de senha</Typography>

                <TextField
                  fullWidth
                  type="password"
                  {...register("passwordConfirmation", { required: true })}
                />
              </Grid>
            )}

            <Grid item xs={12}>
              <Button variant="contained" color="primary" type="submit">
                {formText[currentForm].button}
              </Button>
            </Grid>

            <Grid item xs={12}>
              <Typography variant="caption" component="p">
                {formText[currentForm].toggleText}
              </Typography>
              <Link onClick={toggleForm}>
                {formText[currentForm].toggleLink}
              </Link>
            </Grid>

            {Object.values(formState.errors).map((text, index) => (
              <Typography key={index}>{text.message}</Typography>
            ))}
          </Grid>
        </form>
      </div>
    </div>
  );
};
