import { useState, useEffect } from "react";
import { format } from "date-fns";
import { UserInvestmentService } from "../../services";
import { Report, Loading, Layout } from "../../components";
import StocksImage from "../../assets/images/stocks-image.jpeg";
import { EditIcon, DeleteIcon, CoverImage } from "../alerts/Styled";
import {
  ExportIcon,
  Stock,
  StockTitle,
  StockText,
  StockOptions,
  StockInfo,
  StockLink,
} from "./Styled";
import { Button, Grid, Typography } from "@material-ui/core";
import { InvestmentAlertDTO } from "../../dtos/alertDto";
import {
  CreateUserInvestmentDTO,
  UserInvestmentDTO,
} from "../../dtos/userInvestmentDto";
import { UserInvestmentForm } from "../../components/userInvestmentForm/UserInvestmentForm";

export const UserInvestments = () => {
  const [isLoading, setLoading] = useState(true);
  const [investments, setInvestments] = useState<UserInvestmentDTO[]>([]);

  const [isFormVisible, showForm] = useState(false);
  const [report, showReport] = useState(null);

  useEffect(() => {
    fetchInvestments();
  }, []);

  const fetchInvestments = async () => {
    const { data } = await UserInvestmentService.findAll();
    setInvestments(data);
    setLoading(false);
  };

  const handleSubmit = async (investment: CreateUserInvestmentDTO) => {
    return createInvestment(investment);
  };

  const createInvestment = async (investment: CreateUserInvestmentDTO) => {
    setLoading(true);
    await UserInvestmentService.create(investment);
    setLoading(false);
    showForm(false);
  };

  const editInvestment = async (
    id: number,
    updatedInvestment: Partial<InvestmentAlertDTO>
  ) => {
    setLoading(true);
    await UserInvestmentService.update(id, updatedInvestment);
    setLoading(false);
  };

  const deleteInvestment = async (id: number) => {
    setLoading(true);
    await UserInvestmentService.remove(id);
    setLoading(false);
  };

  const renderInvestments = () => {
    if (isLoading) {
      return null;
    }

    if (investments.length === 0) {
      return (
        <Typography variant="body2">
          Não há nenhum título cadastrado até o momento
        </Typography>
      );
    }

    return investments.map((investment) => {
      const formattedDate = format(new Date(investment.purchasedAt), "MM/yyyy");

      return (
        <Stock key={investment.id}>
          <StockInfo>
            <StockTitle>{investment.type.name}</StockTitle>
            <StockText>{investment.name} | </StockText>
            <StockText>{`Comprado em ${formattedDate}`}</StockText>
          </StockInfo>

          <StockOptions>
            <StockLink onClick={() => showForm(true)}>
              <EditIcon size="1.4em" />
            </StockLink>
            <StockLink onClick={() => deleteInvestment(investment.id)}>
              <DeleteIcon size="1.4em" />
            </StockLink>
          </StockOptions>
        </Stock>
      );
    });
  };

  return (
    <Layout>
      {isLoading && <Loading />}

      {report ? (
        <Report data={report} />
      ) : (
        <>
          <CoverImage image={StocksImage} />

          {isFormVisible ? (
            <UserInvestmentForm
              onSubmit={handleSubmit}
              onCancel={() => showForm(false)}
            />
          ) : (
            <>
              <Grid container alignItems="center" justify="space-between">
                <Grid item>
                  <Typography variant="h3">Meus títulos pessoais</Typography>
                </Grid>
                <Grid item>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => showForm(true)}
                  >
                    Criar novo título
                  </Button>
                </Grid>
              </Grid>

              {renderInvestments()}
            </>
          )}
        </>
      )}
    </Layout>
  );
};
