import AdminPost from "./admin/AdminPost";
import Alerts from "./alerts/Alerts";
import Settings from "./settings/Settings";

export { Landing } from "./landing/Landing";
export { BlogIndex } from "./blog/BlogIndex";
export { BlogPost } from "./blog/BlogPost";
export { Auth } from "./auth/Auth";
export { Home } from "./home/Home";
export { InvestmentSimulator } from "./simulator/InvestmentSimulator";
export { InvestorProfile } from "./investorProfile/InvestorProfile";
export { UserInvestments } from "./userInvestments/UserInvestments";
export { NotFound } from "./notFound/NotFound";

export { AdminPost, Alerts, Settings };
