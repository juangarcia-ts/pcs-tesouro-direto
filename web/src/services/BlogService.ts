import api from "./BaseService";

export const BlogService = {
  findAllPosts: () => api.get(`/posts`),
  findPostById: (id: number) => api.get(`/posts/${id}`),
  createPost: (data: any) => api.post("/posts", data),
  editPost: (data: any) => api.patch("/posts", data),
  removePost: (id: number) => api.delete(`/posts/${id}`),

  findAllComments: (postId: number) => api.get(`/posts/${postId}/comments`),
  createComment: (postId: number, text: string) =>
    api.post(`/posts/${postId}/comments`, { text }),

  replyComment: (postId: number, commentId: string, text: string) =>
    api.patch(`/posts/${postId}/comments/${commentId}`, { text }),
};
