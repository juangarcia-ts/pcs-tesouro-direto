import { LoginDTO, SignUpDTO } from "../dtos/authDto";
import api from "./BaseService";

export const AuthService = {
  login: (data: LoginDTO) => api.post("/auth/login", data),
  signUp: (data: SignUpDTO) => api.post("/auth/signUp", data),
};
