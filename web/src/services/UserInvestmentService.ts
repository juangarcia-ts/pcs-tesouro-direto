import {
  CreateUserInvestmentDTO,
  UserInvestmentDTO,
} from "../dtos/userInvestmentDto";
import api from "./BaseService";

export const UserInvestmentService = {
  findAll: () => api.get(`/users/me/investments`),

  findById: (investmentId: number) =>
    api.get(`/users/me/investments/${investmentId}`),

  create: (newInvestment: CreateUserInvestmentDTO) =>
    api.post(`/users/me/investments`, newInvestment),

  update: (
    investmentId: number,
    updatedInvestment: Partial<UserInvestmentDTO>
  ) => api.patch(`/users/me/investments/${investmentId}`, updatedInvestment),

  remove: (investmentId: number) =>
    api.delete(`/users/me/investments/${investmentId}`),
};
