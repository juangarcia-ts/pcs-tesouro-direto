import api from "./BaseService";
import { UserDTO } from "../dtos/userDto";

export const UserService = {
  editarUsuario: (updatedUser: UserDTO) => api.patch(`/users/me`, updatedUser),
};
