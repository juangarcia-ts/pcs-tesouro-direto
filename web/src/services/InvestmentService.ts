import api from "./BaseService";

export const InvestmentService = {
  getHistory: () => api.get("/investment/history"),
  getTypes: () => api.get("/investment/types"),
};
