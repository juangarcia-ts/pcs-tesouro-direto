import { CreateInvestmentAlertDTO, InvestmentAlertDTO } from "../dtos/alertDto";
import { NotificationType } from "../enums/notificationTypeEnum";
import api from "./BaseService";

export const AlertService = {
  findAll: (investmentId: number) =>
    api.get(`/users/me/investments/${investmentId}/alerts`),

  findById: (investmentId: number, alertId: number) =>
    api.get(`/users/me/investments/${investmentId}/alerts/${alertId}`),

  create: (investmentId: number, newAlert: CreateInvestmentAlertDTO) =>
    api.post(`/users/me/investments/${investmentId}/alerts`, newAlert),

  update: (
    investmentId: number,
    alertId: number,
    updatedAlert: Partial<InvestmentAlertDTO>
  ) =>
    api.patch(
      `/users/me/investments/${investmentId}/alerts/${alertId}`,
      updatedAlert
    ),

  remove: (investmentId: number, alertId: number) =>
    api.delete(`/users/me/investments/${investmentId}/alerts/${alertId}`),

  test: (type: NotificationType) => api.get(`/alerts/test?type=${type}`),
};
