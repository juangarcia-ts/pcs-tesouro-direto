import axios from "axios";
import { getAccessToken } from "../utils/token";

const api = axios.create({
  baseURL: "http://localhost:8080/",
});

api.defaults.headers.common["Accept"] = "application/json";
api.defaults.headers.common["Content-Type"] = "application/json";
api.defaults.timeout = 60000;

api.interceptors.request.use((config) => {
  const token = `Bearer ${getAccessToken()}`;

  api.defaults.headers.common["Authorization"] = token;
  config.headers["Authorization"] = token;

  return config;
});

export default api;
