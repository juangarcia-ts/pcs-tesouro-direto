import DateFnsUtils from "@date-io/date-fns";
import {
  Button,
  Grid,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@material-ui/core";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import ptLocale from "date-fns/locale/pt-BR";
import { Controller, useForm } from "react-hook-form";
import { CreateUserInvestmentDTO } from "../../dtos/userInvestmentDto";
import CurrencyFormat from "react-currency-format";
import { InvestmentTypeDTO } from "../../dtos/investmentTypeDto";
import { useEffect, useState } from "react";
import { InvestmentService } from "../../services";

interface Props {
  onSubmit: (_newInvestment: CreateUserInvestmentDTO) => Promise<void>;
  onCancel: () => void;
}

export const UserInvestmentForm = ({ onSubmit, onCancel }: Props) => {
  const DefaultInput = (props) => <TextField {...props} fullWidth />;
  const [types, setTypes] = useState<InvestmentTypeDTO[]>([]);
  const { register, handleSubmit, control, formState } = useForm();

  useEffect(() => {
    fetchTypes();
  }, []);

  const fetchTypes = async () => {
    const { data } = await InvestmentService.getTypes();
    setTypes(data);
  };

  const normalizedSubmit = (values) => {
    const { amount, paidPrice, yieldRate } = values;

    const data = {
      ...values,
      amount: +amount,
      paidPrice: +paidPrice.replace("R$ ", ""),
      yieldRate: +yieldRate.replace("% a.a", ""),
    };

    onSubmit(data);
  };

  return (
    <form onSubmit={handleSubmit(normalizedSubmit)}>
      <Grid container direction="column" justify="center" spacing={2}>
        <Grid container>
          <Grid item>
            <Typography variant="h3">Novo título</Typography>
          </Grid>
        </Grid>

        <Grid container item xs={12} spacing={4}>
          <Grid item xs={6}>
            <Typography variant="overline">Tipo de investimento</Typography>

            <Controller
              control={control}
              defaultValue=""
              name="typeId"
              rules={{
                required: "Tipo de investimento é obrigatório",
              }}
              render={({ field: { value, onChange } }) => (
                <Select fullWidth value={value} onChange={onChange}>
                  <MenuItem disabled value="">
                    Selecione
                  </MenuItem>

                  {types.map((type) => (
                    <MenuItem key={type.id} value={type.id}>
                      {type.name}
                    </MenuItem>
                  ))}
                </Select>
              )}
            />
          </Grid>

          <Grid item xs={6}>
            <Typography variant="overline">Nome</Typography>

            <TextField
              fullWidth
              {...register("name", { required: "Nome é obrigatório" })}
            />
          </Grid>
        </Grid>

        <Grid container item xs={12} spacing={4}>
          <Grid item xs={3}>
            <Typography variant="overline">Quantidade</Typography>

            <TextField
              fullWidth
              type="number"
              {...register("amount", {
                required: "Quantidade é obrigatória",
                min: { value: 1, message: "Quantidade deve ser maior que 1" },
              })}
            />
          </Grid>

          <Grid item xs={3}>
            <Typography variant="overline">Valor investido</Typography>

            <Controller
              control={control}
              name="paidPrice"
              rules={{
                required: "Valor investido é obrigatório",
              }}
              render={({ field: { value, onChange } }) => (
                <CurrencyFormat
                  value={value}
                  onChange={onChange}
                  customInput={DefaultInput}
                  thousandSeparator=","
                  decimalSeparator="."
                  decimalScale={2}
                  fixedDecimalScale
                  prefix={"R$ "}
                />
              )}
            />
          </Grid>

          <Grid item xs={3}>
            <Typography variant="overline">Taxa de rendimento</Typography>

            <Controller
              control={control}
              name="yieldRate"
              rules={{
                required: "Taxa de rendimento é obrigatória",
              }}
              render={({ field: { value, onChange } }) => (
                <CurrencyFormat
                  value={value}
                  onChange={onChange}
                  customInput={DefaultInput}
                  thousandSeparator=","
                  decimalSeparator="."
                  suffix={"% a.a"}
                />
              )}
            />
          </Grid>

          <Grid item xs={3}>
            <Typography variant="overline">Data de aquisição</Typography>

            <Controller
              control={control}
              name="purchasedAt"
              defaultValue={new Date()}
              rules={{
                required: "Data de aquisição é obrigatória",
              }}
              render={({ field: { value, onChange } }) => (
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={ptLocale}>
                  <KeyboardDatePicker
                    fullWidth
                    disableFuture
                    openTo="month"
                    views={["year", "month"]}
                    value={value}
                    onChange={onChange}
                  />
                </MuiPickersUtilsProvider>
              )}
            />
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Typography variant="overline">Descrição (Opcional)</Typography>
          <TextField fullWidth {...register("description")} />
        </Grid>

        <Grid container justify="flex-end">
          <Grid item>
            <Button size="large" variant="contained" onClick={onCancel}>
              Cancelar
            </Button>
          </Grid>

          <Grid item>
            <Button
              size="large"
              variant="contained"
              color="primary"
              type="submit"
            >
              Confirmar
            </Button>
          </Grid>
        </Grid>

        <Grid container justify="center">
          <Grid item>
            {Object.values(formState.errors).map((text, index) => (
              <Typography
                key={index}
                variant="caption"
                component="p"
                color="error"
              >
                {text.message}
              </Typography>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </form>
  );
};
