import { useHistory } from "react-router-dom";
import { MenuGrid, MenuBox } from "./Styled";
import { menuOptions } from "../../utils/data";

export const HomeMenu = () => {
  const router = useHistory();

  const redirectTo = (link: string) => {
    router.push(link);
  };

  return (
    <MenuGrid>
      {menuOptions.map((option, index) => (
        <MenuBox
          key={index}
          itemsPerRow={2}
          onClick={() => redirectTo(option.link)}
          title={option.title}
          imageURL={option.imageURL}
        />
      ))}
    </MenuGrid>
  );
};
