import Slider from "react-slick";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

interface Props {
  children: JSX.Element | JSX.Element[];
}

const settings = {
  dots: true,
  infinite: true,
  autoplay: true,
  speed: 1000,
  autoplaySpeed: 6000,
};

export const Carrousel = ({ children }: Props) => {
  return <Slider {...settings}>{children}</Slider>;
};
