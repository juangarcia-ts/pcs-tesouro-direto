import Quiz from "./quiz/Quiz";
import Report from "./report/Report";
import Simulator from "./simulator/Simulator";

export { Navbar } from "./navbar/Navbar";
export { Layout } from "./layout/Layout";
export { Carrousel } from "./carrousel/Carrousel";
export { HomeMenu } from "./homeMenu/HomeMenu";
export { Loading } from "./loading/Loading";
export { InvestmentHistory } from "./investmentHistory/InvestmentHistory";

export { Quiz, Report, Simulator };
