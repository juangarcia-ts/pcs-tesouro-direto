import {
  Tabs,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import { useState } from "react";
import { InvestmentHistoryDTO } from "../../dtos/investmentHistoryDto";
import { InvestmentGroup } from "../../enums/investmentGroupEnum";
import { formatCurrency } from "../../utils/currency";
import useStyles from "./InvestmentHistory.styles";

interface Props {
  history: InvestmentHistoryDTO;
}

export const InvestmentHistory = ({ history }: Props) => {
  const classes = useStyles();
  const [group, setGroup] = useState(InvestmentGroup.DEPOSIT);

  const data = history[group];

  const renderRows = () => {
    return data.map((investment) => (
      <TableRow key={investment.id}>
        <TableCell>
          <Typography variant="overline">{investment.fullName}</Typography>
        </TableCell>
        <TableCell align="right">
          <Typography variant="overline">{investment.yieldRate}</Typography>
        </TableCell>
        <TableCell align="right">
          <Typography variant="overline">
            {formatCurrency(investment.unitPrice)}
          </Typography>
        </TableCell>
        {group === InvestmentGroup.DEPOSIT && (
          <TableCell align="right">
            <Typography variant="overline">
              {formatCurrency(investment.minValue)}
            </Typography>
          </TableCell>
        )}
      </TableRow>
    ));
  };

  return (
    <section className={classes.container}>
      <Tabs
        value={group}
        indicatorColor="primary"
        textColor="primary"
        onChange={(_, newGroup) => setGroup(newGroup)}
        className={classes.tabs}
      >
        <Tab label="Investimento" value={InvestmentGroup.DEPOSIT} />
        <Tab label="Resgate" value={InvestmentGroup.WITHDRAW} />
      </Tabs>

      <TableContainer>
        <Table size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>
                <Typography variant="caption">Nome</Typography>
              </TableCell>
              <TableCell align="right">
                <Typography variant="caption">Taxa de rendimento</Typography>
              </TableCell>
              <TableCell align="right">
                <Typography variant="caption">Valor unitário</Typography>
              </TableCell>
              {group === InvestmentGroup.DEPOSIT && (
                <TableCell align="right">
                  <Typography variant="caption">Investimento mínimo</Typography>
                </TableCell>
              )}
            </TableRow>
          </TableHead>

          <TableBody>{renderRows()}</TableBody>
        </Table>
      </TableContainer>
    </section>
  );
};
