import { makeStyles } from "@material-ui/core";

export default makeStyles(() => ({
  container: {
    margin: "48px 0px",
  },
  tabs: {
    marginBottom: 24,
  },
  table: {
    overflowX: "hidden",
  },
}));
