import { makeStyles, Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
  navbar: {
    padding: 24,
    top: 0,
    left: 0,
    right: 0,
    position: "fixed",
    backgroundColor: theme.palette.common.white,
    zIndex: theme.zIndex.appBar,
  },
}));
