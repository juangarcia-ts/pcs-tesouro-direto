import { Container, Grid, Link, Typography } from "@material-ui/core";
import { Role } from "../../dtos/authDto";
import { useLoggedUser } from "../../hooks/useLoggedUser";
import { logoutUser } from "../../utils/token";
import useStyles from "./Navbar.styles";

const pages = [
  { title: "Home", route: "/", roles: [] },
  {
    title: "Home",
    route: "/pagina-inicial",
    roles: [Role.CUSTOMER],
  },
  {
    title: "Publicações",
    route: "/admin/blog",
    roles: [Role.ADMIN],
  },
  { title: "Blog", route: "/blog" },
  { title: "Entrar", route: "/entrar" },
];

export const Navbar = () => {
  const classes = useStyles();
  const loggedUser = useLoggedUser();

  const renderPages = () => {
    const list = pages.filter((page) => {
      if (!page.roles || (!loggedUser && page.roles.length === 0)) {
        return page;
      }

      if (page.roles.includes(loggedUser?.role)) {
        return page;
      }

      return null;
    });

    return list.map((page, index) => {
      if (loggedUser && page.route === "/entrar") {
        return null;
      }

      return (
        <Grid item key={index}>
          <Link href={page.route}>
            <Typography variant="body1">{page.title}</Typography>
          </Link>
        </Grid>
      );
    });
  };

  return (
    <nav className={classes.navbar}>
      <Container>
        <Grid container>
          <Grid item xs>
            <Link href="/">
              <Typography color="primary" variant="h6" component="h1">
                MeuTesouro
              </Typography>
            </Link>
          </Grid>

          <Grid
            container
            item
            xs
            alignItems="center"
            justify="flex-end"
            spacing={4}
          >
            {renderPages()}

            {loggedUser && (
              <Grid item>
                <Link href="/" onClick={logoutUser}>
                  <Typography variant="body1">Sair</Typography>
                </Link>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Container>
    </nav>
  );
};
