import { makeStyles } from "@material-ui/core";

export default makeStyles(() => ({
  loading: {
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#000",
    color: "#fff",
    opacity: 0.7,
    zIndex: 10003,
  },
}));
