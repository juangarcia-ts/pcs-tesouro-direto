import { CircularProgress, Grid, Typography } from "@material-ui/core";
import useStyles from "./Loading.styles";

export const Loading = () => {
  const classes = useStyles();

  return (
    <Grid
      container
      direction="column"
      alignItems="center"
      justify="center"
      spacing={4}
      className={classes.loading}
    >
      <Grid item>
        <CircularProgress size={50} color="primary" />
      </Grid>

      <Grid item>
        <Typography variant="caption">Carregando...</Typography>
      </Grid>
    </Grid>
  );
};
