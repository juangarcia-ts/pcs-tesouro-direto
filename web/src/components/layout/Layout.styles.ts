import { makeStyles } from "@material-ui/core";

export default makeStyles(() => ({
  main: {
    position: "absolute",
    top: 100,
    left: 0,
    right: 0,
  },
}));
