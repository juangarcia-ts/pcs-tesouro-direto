import { Container } from "@material-ui/core";
import useStyles from "./Layout.styles";

interface Props {
  children: JSX.Element | JSX.Element[];
}

export const Layout = ({ children }: Props) => {
  const classes = useStyles();

  return (
    <main className={classes.main}>
      <Container>{children}</Container>
    </main>
  );
};
