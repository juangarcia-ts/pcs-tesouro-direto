import jwtDecode from "jwt-decode";
import { useContext } from "react";
import { AuthContext } from "../contexts/authContext";
import { AuthUserDTO } from "../dtos/authDto";

export const useLoggedUser = (): AuthUserDTO | undefined => {
  const { token } = useContext(AuthContext);

  if (!token) {
    return;
  }

  return jwtDecode(token);
};
