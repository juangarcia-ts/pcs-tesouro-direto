# Tesouro Direto

API desenvolvida para a disciplina PCS-SGBD da Unirio (Prof: Fabricio Pereira).

## Pré-requisitos

- Node
- Docker
- make (Opcional)

## Inicializando a aplicação

- Primeiramente, instale as dependências utilizando `yarn` ou `npm install` em cada projeto

- Suba os bancos de dados com o comando `make up` ou `docker-compose up -d`

- Execute os scripts SQL com o `yarn db:update` ou `npm run db:update` dentro da pasta da api

- Execute as aplicações com `yarn dev` ou `npm run dev`

## Swagger

Para visualizar a documentação do Swagger, inicialize a API e acesse `localhost:8080/docs`
